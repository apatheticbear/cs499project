/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CourseScheduler.Exceptions;

/**
 *
 * @author Zach
 */
public class DepartmentException extends Exception {
    public DepartmentException(){
        
    }
    
    /**
     *
     * @param msg
     */
    public DepartmentException(String msg){
        super(msg);
    }
}

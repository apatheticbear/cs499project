/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CourseScheduler.Exceptions;

/**
 *
 * @author Zach
 */
public class FileParserException extends Exception{
    
    public FileParserException(){
        
    }
    
    public FileParserException(String msg){
        super(msg);
    }
}

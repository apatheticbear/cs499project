/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CourseScheduler.Exceptions;

/**
 *
 * @author Zach
 */
public class SchedulingException extends Exception{
    
    public SchedulingException(){
        
    }
    
    public SchedulingException(String msg){
        super(msg);
    }
}

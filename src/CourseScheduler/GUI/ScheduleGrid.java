package CourseScheduler.GUI;

import CourseScheduler.UniversityParts.*;

import java.util.ArrayList;

public class ScheduleGrid {

    ArrayList<TimePeriod> timeColumns; //Columns
    ArrayList<ArrayList<Cell>> grid = new ArrayList<>();
    University uni;

    //When a ScheduleGrid_old object is created, it will automatically populate the cells based on the passed in days, time blocks, and department
    public ScheduleGrid(ArrayList<TimePeriod> timeColumns, University uni) {
        this.timeColumns = timeColumns;
        this.uni = uni;

        //Create the grid
        for (int i = 0; i < 2; i++)
        {
            //for every day, create an arrayList of the time blocks
            grid.add(new ArrayList<Cell>());

            //for every timePeriod in the day, add cells
            for (int j = 0; j < timeColumns.size(); j++)
            {
                //Add a new cell to the 2D ArrayList
                grid.get(i).add(new Cell());
            }
        }
    }

    //Return a specific cell in the grid
    public Cell getCell(int row, int column) {
        return grid.get(row).get(column);
    }

    //populate all cells with department rooms
    public void populateCellsWithRooms() {
        //for every day
        for (int i = 0; i < 2; i++) {
            //for every timePeriod
            for (int j = 0; j < timeColumns.size(); j++) {
                //for rooms in the university
                for (int k = 0; k < this.uni.getRoomList().size(); k++) {
                    //input integers to represent the index of the room
                    getCell(i, j).addRoom(this.uni.getRoomList().get(k));
                }
            }
        }
    }

    //Assign preferences to instructor to assist in scheduling
    public void assignInstructorPreferences() {
        ArrayList<Integer> morningPeriods = new ArrayList<>();
        morningPeriods.add(0);
        morningPeriods.add(1);

        ArrayList<Integer> midDayPeriods = new ArrayList<>();
        midDayPeriods.add(2);
        midDayPeriods.add(3);
        midDayPeriods.add(4);

        ArrayList<Integer> eveningPeriods = new ArrayList<>();
        eveningPeriods.add(5);
        eveningPeriods.add(6);

        //Assign instructors with preferences first
        for (int i = 0; i < this.uni.getInstructorList().size(); i++) //iterate through instructors
        {
            //Check morning preference
            if (this.uni.getInstructorList().get(i).getPreference().equalsIgnoreCase("Morning")) {
                for (int j = 0; j < this.uni.getInstructorList().get(i).getCourseList().size(); j++) {
                    this.uni.getInstructorList().get(i).getCourseList().get(j).setPreferredTimePeriod(morningPeriods);
                }
            } //Check mid-day preference
            else if (this.uni.getInstructorList().get(i).getPreference().equalsIgnoreCase("Mid-day")) {
                for (int j = 0; j < this.uni.getInstructorList().get(i).getCourseList().size(); j++) {
                    this.uni.getInstructorList().get(i).getCourseList().get(j).setPreferredTimePeriod(midDayPeriods);
                }
            } //Check evening preference
            else if (this.uni.getInstructorList().get(i).getPreference().equalsIgnoreCase("Evening")) {
                for (int j = 0; j < this.uni.getInstructorList().get(i).getCourseList().size(); j++) {
                    this.uni.getInstructorList().get(i).getCourseList().get(j).setPreferredTimePeriod(eveningPeriods);
                }
            }

        }
    }

    //Assign preferences to rooms to assist in scheduling
    public void assignRoomPreferences() {
        for (int i = 0; i < this.uni.getCourseList().size(); i++) {
            //If there is no preferences, give it the preferred Room ID of -1 to make scheduling easier
            if (this.uni.getCourseList().get(i).getPreferredRoomNum().equals("")) {
                this.uni.getCourseList().get(i).setPreferredRoomID(-1);
            } //Set a preferred Room ID so we only schedule the course to that specific room
            else {
                for (int j = 0; j < this.uni.getRoomList().size(); j++) {
                    if (this.uni.getCourseList().get(i).getPreferredRoomNum().equalsIgnoreCase(this.uni.getRoomList().get(j).getRoomNum()) && this.uni.getCourseList().get(i).getPreferredBuilding().equalsIgnoreCase(this.uni.getRoomList().get(j).getBuildingName())) {
                        //System.out.println(this.department.getCourseList().get(i).getClassName() + " " + this.department.getRoomList().get(j).getRoomNum());
                        this.uni.getCourseList().get(i).setPreferredRoomID(j);
                        break;
                    }
                }
            }
        }
    }

    public void schedule(){
        //go through the instructors the first time
        for (int i = 0; i < uni.getInstructorList().size(); i++) {
            //go through the course list of the instructor
            for (int j = 0; j < uni.getInstructorList().get(i).getCourseList().size(); j++) {
                //only schedule ones with room preferences first
                if (uni.getInstructorList().get(i).getCourseList().get(j).getReqClassroom() == true) {
                    //if the instructor has a time preference
                    if (!(uni.getInstructorList().get(i).getPreference().equalsIgnoreCase("none"))) {
                        ArrayList<Integer> indices = this.findRoom_AllPrefs(uni.getInstructorList().get(i).getCourseList().get(j).getPreferredTimePeriod(), uni.getInstructorList().get(i).getCourseList().get(j).getPreferredRoomID(), i);
                            uni.getInstructorList().get(i).getCourseList().get(j).setDayAndTimePeriod(indices.get(0), indices.get(1));
                            uni.getInstructorList().get(i).getCourseList().get(j).setAssignedRoom(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)));
                            uni.getInstructorList().get(i).getCourseList().get(j).setBuildingName(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getBuildingName());
                            uni.getInstructorList().get(i).getCourseList().get(j).setRoomNum(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getRoomNum());
                            getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).addCourse(
                                    uni.getInstructorList().get(i).getCourseList().get(j), //Course to add
                                    uni.getInstructorList().get(i).getCourseList().get(j).getDayNumber(), // Day to add course
                                    uni.getInstructorList().get(i).getCourseList().get(j).getTimePeriod().getPeriodNumber()); //period to add course
                            getCell(indices.get(0),indices.get(1)).addInstructorToCell(indices.get(0), indices.get(1), indices.get(2), i);
                    }
                }
            }
        }
        
        //go through the instructors the again 
        for (int i = 0; i < uni.getInstructorList().size(); i++) {
            //go through the course list of the instructor
            for (int j = 0; j < uni.getInstructorList().get(i).getCourseList().size(); j++) {
                //only schedule ones with room preferences first
                if (uni.getInstructorList().get(i).getCourseList().get(j).getReqClassroom() == true) {
                    //if the instructor has a time preference
                    if (uni.getInstructorList().get(i).getPreference().equalsIgnoreCase("none")) {
                        ArrayList<Integer> indices = this.findRoom_RoomPref(uni.getInstructorList().get(i).getCourseList().get(j).getPreferredRoomID(), i);
                            uni.getInstructorList().get(i).getCourseList().get(j).setDayAndTimePeriod(indices.get(0), indices.get(1));
                            uni.getInstructorList().get(i).getCourseList().get(j).setAssignedRoom(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)));
                            uni.getInstructorList().get(i).getCourseList().get(j).setBuildingName(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getBuildingName());
                            uni.getInstructorList().get(i).getCourseList().get(j).setRoomNum(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getRoomNum());
                            getCell(indices.get(0),indices.get(1)).addInstructorToCell(indices.get(0), indices.get(1), indices.get(2), i);
                    }
                }
            }   
        }
        
        //go through the instructors the third time
        for(int i = 0; i < uni.getInstructorList().size(); i++){
            //go through the course list of instructors again
            for(int j = 0; j < uni.getInstructorList().get(i).getCourseList().size(); j++){
                //schedule the one's without room preferences
                if (uni.getInstructorList().get(i).getCourseList().get(j).getReqClassroom() == false) {
                    //System.out.println(uni.getInstructorList().get(i).getId());
                    //if instructor has time preference
                    if (!(uni.getInstructorList().get(i).getPreference().equalsIgnoreCase("none"))) {
                        ArrayList<Integer> indices = this.findRoom_TimePref(uni.getInstructorList().get(i).getCourseList().get(j).getPreferredTimePeriod(), uni.getInstructorList().get(i).getCourseList().get(j).getDepartmentName(), i);
                            uni.getInstructorList().get(i).getCourseList().get(j).setDayAndTimePeriod(indices.get(0), indices.get(1));
                            uni.getInstructorList().get(i).getCourseList().get(j).setAssignedRoom(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)));
                            uni.getInstructorList().get(i).getCourseList().get(j).setBuildingName(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getBuildingName());
                            uni.getInstructorList().get(i).getCourseList().get(j).setRoomNum(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getRoomNum());
                            getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).addCourse(
                                        uni.getInstructorList().get(i).getCourseList().get(j), //Course to add
                                         uni.getInstructorList().get(i).getCourseList().get(j).getDayNumber(), // Day to add course
                                        uni.getInstructorList().get(i).getCourseList().get(j).getTimePeriod().getPeriodNumber()); //period to add course
                            getCell(indices.get(0),indices.get(1)).addInstructorToCell(indices.get(0), indices.get(1), indices.get(2), i);
                    }
                }
            }
        }
        
        //go through the instructors the final time
        for(int i = 0; i < uni.getInstructorList().size(); i++){
            //go through the course list of instructors again
            for(int j = 0; j < uni.getInstructorList().get(i).getCourseList().size(); j++){
                //schedule the one's without room preferences
                if (uni.getInstructorList().get(i).getCourseList().get(j).getReqClassroom() == false) {
                    //if instructor has no time preference
                    if (uni.getInstructorList().get(i).getPreference().equalsIgnoreCase("none")) {
                        ArrayList<Integer> indices = this.findRoom_NoPrefs(uni.getInstructorList().get(i).getCourseList().get(j).getDepartmentName(), i);
                            uni.getInstructorList().get(i).getCourseList().get(j).setDayAndTimePeriod(indices.get(0), indices.get(1));
                            uni.getInstructorList().get(i).getCourseList().get(j).setAssignedRoom(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)));
                            uni.getInstructorList().get(i).getCourseList().get(j).setBuildingName(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getBuildingName());
                            uni.getInstructorList().get(i).getCourseList().get(j).setRoomNum(getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).getRoomNum());
                            getCell(indices.get(0),indices.get(1)).getRoomByIndex(indices.get(2)).addCourse(
                                    uni.getInstructorList().get(i).getCourseList().get(j), //Course to add
                                    uni.getInstructorList().get(i).getCourseList().get(j).getDayNumber(), // Day to add course
                                    uni.getInstructorList().get(i).getCourseList().get(j).getTimePeriod().getPeriodNumber()); //period to add course
                            getCell(indices.get(0),indices.get(1)).addInstructorToCell(indices.get(0), indices.get(1), indices.get(2), i);
                    }
                }
            }
        }
    }
    
    /**
     * 
     * @param prefPeriods the time periods the instructor prefers
     * @param prefRoom the room the course requires
     * @param prefBuilding the building of the room the course requires
     * @return 
     */
    private ArrayList<Integer> findRoom_AllPrefs(ArrayList<Integer> prefPeriods, int prefRoomIndex, int teacherIndex){
        //go through the days
        for (int i = 1; i > -1; i--) {
            //go through the instructor's preferred times
            for (int period : prefPeriods) {
                //if the room isn't assigned at this time
                if (getCell(i, period).getIsAssignedAtRoomIndex(prefRoomIndex) == false){
                    if(this.areTeacherTimesCorrect(i, period, teacherIndex) == true){
                        //set the availability at this spot to false
                        getCell(i, period).setRoomIsAssigned(prefRoomIndex, true);
                        ArrayList<Integer> indices = new ArrayList();
                        indices.add(i);
                        indices.add(period);
                        indices.add(prefRoomIndex);
                        return indices;
                    }
                }
            }
        }
        
        //
        ////if it makes it here, nothing is available in this room at preferred time slot
        ////go through it again without teacher time preferences
        ////go through the days
        //for (int i = 0; i < 2; i++){
        //    //go through the time periods
        //    for(int j = 0; j < this.timeColumns.size(); j++){
        //        //find if the room is available or not
        //        if(getCell(i,j).getIsAssignedAtRoomIndex(prefRoomIndex) == false){
        //            if(this.areTeacherTimesCorrect(i, j, teacherIndex) == true){
        //                //set the availability at this spot to false
        //                getCell(i, j).setRoomIsAssigned(prefRoomIndex, true);
        //                ArrayList<Integer> indices = new ArrayList();
        //                indices.add(i);
        //                indices.add(j);
        //                indices.add(prefRoomIndex);
        //                return indices;
        //            }
        //        }
        //    }
        //}
        
        //if it makes it here, then the required room doesn't have time slots available
        return null;
    }
    
    /**
     * 
     * @param prefRoomIndex
     * @return null if no time slots available for required room
     * @return indices arrayList of integers that contains, row column, and roomIndex
     */
    private ArrayList<Integer> findRoom_RoomPref(int prefRoomIndex, int teacherIndex){
        //go through the days
        for (int i = 1; i > -1; i--){
            //go through the time periods
            for(int j = 0; j < this.timeColumns.size(); j++){
                //find if the room is available or not
                if(getCell(i,j).getIsAssignedAtRoomIndex(prefRoomIndex) == false){
                    if(this.areTeacherTimesCorrect(i, j, teacherIndex) == true){
                        //set the availability at this spot to false
                        getCell(i, j).setRoomIsAssigned(prefRoomIndex, true);
                        ArrayList<Integer> indices = new ArrayList();
                        indices.add(i);
                        indices.add(j);
                        indices.add(prefRoomIndex);
                        return indices;
                    }
                }
            }
        }
        //returns null if no time slots available for required room
        return null;
    }
    
    private ArrayList<Integer> findRoom_TimePref(ArrayList<Integer> prefPeriods,String departmentName, int teacherIndex){
        //go through the days
        for(int i = 1; i > -1; i--){
            //go through the time periods
            for(int Period : prefPeriods){
                //go through the rooms in the cell
                //for(Room roomm : getCell(i, Period).getRooms()){
                Department dept = uni.findDepartment(departmentName);
                if(dept != null) {
                    //go through this department's buildings
                   for(Building bldg : dept.getBuildingList()) {
                       //go through the building's rooms and get the index in the university's room list
                       for(int i_room = 0; i_room < bldg.getRoomList().size(); i_room++){
                           //if the room is available
                           if(getCell(i,Period).getIsAssignedAtRoomIndex(bldg.getRoomList().get(i_room).getId()) == false){
                               //check if the times are okay for this teacher 
                               if(this.areTeacherTimesCorrect(i, Period, teacherIndex) == true){
                                    //set the availability at this spot to false
                                    getCell(i, Period).setRoomIsAssigned(bldg.getRoomList().get(i_room).getId(), true);
                                    ArrayList<Integer> indices = new ArrayList();
                                    indices.add(i);
                                    indices.add(Period);
                                    indices.add(bldg.getRoomList().get(i_room).getId());
                                    return indices;
                                }
                           }
                       }
                   }
                }
            }
        }
        
        ////go through it again without teacher time preferences
        ////go through the days
        //for (int i = 0; i < 2; i++){
        //    //go through the time periods
        //    for(int j = 0; j < this.timeColumns.size(); j++){
        //        //go through the rooms in the cell
        //        //for(Room roomm : getCell(i, Period).getRooms()){
        //        Department dept = uni.findDepartment(departmentName);
        //        if(dept != null) {
        //            //go through this department's buildings
        //           for(Building bldg : dept.getBuildingList()) {
        //               //go through the building's rooms and get the index in the university's room list
        //               for(int i_room = 0; i_room < bldg.getRoomList().size(); i_room++){
        //                   //if the room is available
        //                   if(getCell(i,j).getIsAssignedAtRoomIndex(bldg.getRoomList().get(i_room).getId()) == false){
        //                       //check if the times are okay for this teacher 
        //                       if(this.areTeacherTimesCorrect(i, j, teacherIndex) == true){
        //                            //set the availability at this spot to false
        //                            getCell(i, j).setRoomIsAssigned(bldg.getRoomList().get(i_room).getId(), true);
        //                            ArrayList<Integer> indices = new ArrayList();
        //                            indices.add(i);
        //                            indices.add(j);
        //                            indices.add(bldg.getRoomList().get(i_room).getId());
        //                            return indices;
        //                        }
        //                   }
        //               }
        //           }
        //       }
        //    }
        //}
        ////not enough rooms in the department to handle all courses
        return null;
    }
    
    private ArrayList<Integer> findRoom_NoPrefs(String departmentName, int teacherIndex){
        //go through days
        for(int i = 0; i < 2; i++){
            //go through the time periods of the day
            for(int j = 0; j < this.timeColumns.size(); j++){
                Department dept = uni.findDepartment(departmentName);
                if(dept != null) {
                    //go through this department's buildings
                   for(Building bldg : dept.getBuildingList()) {
                       //go through the building's rooms and get the index in the university's room list
                       for(int i_room = 0; i_room < bldg.getRoomList().size(); i_room++){
                           //if the room is available
                           if(getCell(i,j).getIsAssignedAtRoomIndex(bldg.getRoomList().get(i_room).getId()) == false){
                                //check if the times are okay for this teacher 
                               if(this.areTeacherTimesCorrect(i, j, teacherIndex) == true){
                                    //set the availability at this spot to false
                                    getCell(i, j).setRoomIsAssigned(bldg.getRoomList().get(i_room).getId(), true);
                                    ArrayList<Integer> indices = new ArrayList();
                                    indices.add(i);
                                    indices.add(j);
                                    indices.add(bldg.getRoomList().get(i_room).getId());
                                    return indices;
                               }
                           }
                       }
                   }
                }
            }
        }
        //not enough rooms in the department to handle all courses
        return null;
    }
    
    private boolean areTeacherTimesCorrect(int row, int column, int teacherIndex){
        //go through the rooms in the cell
        for(int i =0; i < getCell(row,column).getRooms().size(); i++){
            //check if the room at this index has the corresponding index of the given teacher
            if(getCell(row,column).getInstructorAtRoomIndex(i) == teacherIndex){
                return false;
            }
        }
        //if it makes it here, this means the teacher isn't teaching any classes at this same time
        return true;
    }
}

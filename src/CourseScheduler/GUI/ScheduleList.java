package CourseScheduler.GUI;
/*
    Created by Tristan Davis
    CSV/TXT Writing by Hayley Johnsey
        This class will create a table listing the schedule data.
 */
import CourseScheduler.UniversityParts.Course;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Formatter;

public class ScheduleList extends HBox {
    private TableView table;
    //Variables for registering conflicts for various display reasons.
    private PseudoClass highlightRow = PseudoClass.getPseudoClass("highlightList");
    boolean conflictColumnInit = false; //To prevent the conflict column from continually unhiding itself.
    TableColumn columnConflict;
    private ArrayList<Course> courses;

    ScheduleList(ArrayList<Course> data) {
        courses = data;
        String[] properties = {"Class", "Room", "Time", "Day", "Professor", "Building", "Department", "Conflict", "Capacity", "Enrollment"};
        table = new TableView();
        VBox vbox = new VBox();
        ObservableList obs_course;
       // columns = new TableColumn[properties.length];
        TableColumn columnClass = new TableColumn(properties[0]);
        columnClass.setCellValueFactory(new PropertyValueFactory<Course, String>("classAndSection"));
        TableColumn columnRoom = new TableColumn(properties[1]);
        columnRoom.setCellValueFactory(new PropertyValueFactory<Course, String>("roomNum"));
        TableColumn columnTime = new TableColumn(properties[2]);
        columnTime.setCellValueFactory(new PropertyValueFactory<Course, String>("stringClassTime"));
        TableColumn columnDay = new TableColumn(properties[3]);
        columnDay.setCellValueFactory(new PropertyValueFactory<Course, String>("days"));
        TableColumn columnProf = new TableColumn(properties[4]);
        columnProf.setCellValueFactory(new PropertyValueFactory<Course, String>("professorFullName"));
        TableColumn columnBuilding = new TableColumn(properties[5]);
        columnBuilding.setCellValueFactory(new PropertyValueFactory<Course, String>("buildingName"));
        TableColumn columnDepartment = new TableColumn(properties[6]);
        columnDepartment.setCellValueFactory(new PropertyValueFactory<Course, String>("departmentName"));
        TableColumn columnCapacity = new TableColumn(properties[8]);
        columnCapacity.setCellValueFactory(new PropertyValueFactory<Course, Integer> ("capacity"));
        TableColumn columnEnrollment = new TableColumn(properties[9]);
        columnEnrollment.setCellValueFactory(new PropertyValueFactory<Course, Integer>("expectedEnrollment"));
        columnConflict = new TableColumn(properties[7]);
        columnConflict.setCellValueFactory(new PropertyValueFactory<Course, String>("conflict"));

        obs_course =  FXCollections.observableList(data);
        //Check for conflicts
        int courseIndex = 0;
        boolean conflictFound = false;
        while(courseIndex < data.size() && conflictFound == false) {
            if(data.get(courseIndex).getConflict()) {
                conflictFound = true;
            }
            courseIndex++;
        }

       // Insert columns into tables
        table.getColumns().addAll(columnClass, columnProf, columnTime, columnDay, columnRoom, columnBuilding, columnDepartment, columnCapacity, columnEnrollment, columnConflict);
        table.setItems(obs_course);
        table.setTableMenuButtonVisible(true);

        //By default hide the conflict column; there may not be any conflicts.
        if(conflictFound == false) {
            columnConflict.setVisible(false);
        }
        else {
            columnConflict.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(columnConflict);
        }

        vbox.getChildren().add(table);
        //Bind width and height of the table to the vbox
        table.prefWidthProperty().bind(vbox.widthProperty());
        table.prefHeightProperty().bind(vbox.heightProperty());

        //Highlight rows which have a conflict.
        table.setRowFactory(tv -> new TableRow<Course>() {


            @Override
            public void updateItem(Course item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    pseudoClassStateChanged(highlightRow, false);
                } else {
                    updateState(this);
                }
            }
        });

        //Bind the vbox to the hbox (this, ScheduleGantt)
        vbox.prefWidthProperty().bind(this.widthProperty());
        vbox.prefHeightProperty().bind(this.heightProperty());
        this.getChildren().add(vbox);

    }

    public void printReport(String dirPath) {
        
        PrintWriter pw = null;  //TXT
        PrintWriter pw2 = null; //CSV
        try {
            pw = new PrintWriter(dirPath + "/" + "PrintedReport.txt");
            pw2 = new PrintWriter(dirPath + "/" + "PrintedReport.csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        ObservableList<TableColumn> columns = table.getColumns();
        StringBuilder sb = new StringBuilder();     //CSV
        StringBuilder sb2 = new StringBuilder();    //TXT
        Formatter fmt = new Formatter(sb2);         //TXT
        //Print headers
        for (TableColumn col : columns) {
            if (col.isVisible()) {
                sb.append(col.getText() + ",");
                
                fmt.format("%-25s ", col.getText());
                //sb2.append(headers);
                //pw.printf(col.getText() + ",");
            }
        }
        sb.append("\n");
        sb2.append("\r\n");
        sb2.append("_________________________ _________________________ _________________________ _________________________ _________________________ _________________________ _________________________");
        sb2.append("\r\n");
        //pw.println("");


     //Print values
        for(int i = 0; i < table.getItems().size(); i++) {
            for (TableColumn col : columns) {
                if (col.isVisible()) {
                    //pw.print(col.getCellObservableValue(i).getValue() + ",");
                    sb.append(col.getCellObservableValue(i).getValue() + ",");
                    
                    fmt.format("%-25s ", col.getCellObservableValue(i).getValue());
                    //sb2.append(values);
                }
            }
            sb.append("\n");
            sb2.append("\r\n");
            //pw.println("");

        }
        //System.out.println(sb);
        pw.write(sb2.toString());
        pw2.write(sb.toString());
        pw.close();
        pw2.close();
        //System.out.println("DONE WRITING REPORTS!");
    }

    //Update the state of the row; this highlights the row
    private <T> void updateState(TableRow<T> row) {
        TableView<T> table = row.getTableView() ;
        T item = row.getItem();
        Course c = (Course) item;
        c.setConflict(false);
        //Check for conflicts
        for(Course course : courses) {
            if(!course.getClassAndSection().equals(c.getClassAndSection()) && //CLass is not compared to itself
                    course.getRoomInfo().equals(c.getRoomInfo()) && //class is in the same room
                    course.getDays().equals(c.getDays()) && //class is on the same day
                    (course.getTimePeriod().getPeriodNumber() == c.getTimePeriod().getPeriodNumber()) ) //class is at the same time
            {
                    course.setConflict(true);
                    c.setConflict(true);
            }

        }

        //If the item is null there is no conflict
        if (item == null ) {
            row.pseudoClassStateChanged(highlightRow, false);
            return ;
        }

        //If conflict is true, highlight row and make conflict column visible
        if (c.getConflict()) {
            row.pseudoClassStateChanged(highlightRow, true);
            if(!conflictColumnInit) {
                columnConflict.setVisible(true);
                conflictColumnInit = true;
            }
            return ;
        }


        // if we got this far, clear both pseudoclasses:
        row.pseudoClassStateChanged(highlightRow, false);
    }
}

package CourseScheduler.GUI;

import CourseScheduler.Exceptions.FileParserException;
import CourseScheduler.Exceptions.SchedulingException;
import CourseScheduler.MVC.SchedulingSystemModel;
import CourseScheduler.UniversityParts.Course;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import CourseScheduler.UniversityParts.Room;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.text.*;
import javafx.util.converter.IntegerStringConverter;
/**
 *
 * @author Zach
 * Modifications Made by Hayley Johnsey
 */
public class FileLoaderGUI extends Application {

    public void initialize(SchedulingSystemModel ssm) {
        this.ssm = ssm;
    }

    @Override
    public void start(Stage primaryStage) {
        //Create the tab pane
        TabPane tp = new TabPane();

        SchedulingSystemModel ssm = SchedulingSystemModel.getInstance();
        //the table to hold the CSV data
        table = new TableView();
        table.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        table.setEditable(true);

        //add columns to the table
        TableColumn<Course, String> classColumn = new TableColumn("Class");
        TableColumn<Course, Integer> sectionColumn = new TableColumn("Section");
        TableColumn<Course, String> professorColumn = new TableColumn("Professor");
        TableColumn<Course, Integer> capacityColumn = new TableColumn("Max Capacity");
        TableColumn<Course, Integer> enrollmentColumn = new TableColumn("Enrollment");
        TableColumn<Course, String> timeColumn = new TableColumn("Time");
        TableColumn<Course, String> dayColumn = new TableColumn("Day");
        TableColumn<Course, String> roomColumn = new TableColumn("Room");
        TableColumn<Course, String> buildingColumn = new TableColumn("Building");
        TableColumn<Course, String> departmentColumn = new TableColumn("Department");

        //*********allows for changing of cells manually******************
        classColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        classColumn.setOnEditCommit((CellEditEvent<Course, String> t) -> {
            String oldVal = t.getOldValue();
            boolean isFound = false;
            //look to see if the entered class name is an actual class
            for (int i = 0; i < ssm.getDepartmentList().size(); i++) {
                for (int j = 0; j < ssm.getDepartmentList().get(i).getCourseList().size(); j++) {
                    //if the entered class name is found
                    if (t.getNewValue().equalsIgnoreCase(ssm.getDepartmentList().get(i).getCourseList().get(j).getClassName())){
                        isFound = true;
                        break;
                    }
                }
            }
            
            //if it is not an actual class name, then put the original value back in the cell
            if(isFound == false){
                t.getRowValue().setClassName(oldVal);
                //Create classError message for TextFlow
                Text classError = new Text("Invalid Input in Class Name Cell_old. This is not a given class name\n");
                //Set classError message text fill to red
                classError.setFill(Color.rgb(225,10,10));
                //Add classError message to TextFlow
                fileNameTextArea.getChildren().add(classError);
            }
            
            else { //Else, replace original value with new input
                String newVal = t.getNewValue().toUpperCase();
                t.getRowValue().setClassName(newVal);
            }
            //refresh the table to make sure changes are shown
            this.refreshTable();
        });
        
        capacityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        capacityColumn.setOnEditCommit((CellEditEvent<Course, Integer> t) -> {
            //if it's an integer that's typed in
                int newCap = t.getNewValue();
                int enrollment = t.getRowValue().getExpectedEnrollment();
                //if the newly-typed apacity is less than enrollment and that capacity is not negative
                if((newCap < enrollment) && !(newCap < 0)){
                    //Create roomError message for TextFlow
                    Text capacityError = new Text("******Warning****** Enrollment for this course ("  + t.getRowValue().getClassName() + ") is greater than the capacity! \n");
                    //Set roomError text fill to red
                    capacityError.setFill(Color.rgb(225,10,10));
                    //Add roomError message to TextFlow
                    fileNameTextArea.getChildren().add(capacityError);
                }
                //else if the newly-typed capacity is negative but not less than enrollment
                else if((newCap < 0) && !(newCap < enrollment)) {
                    //Create capError message for TextFlow
                    Text capError = new Text("Capacity cannot be less than 0.\n");
                    //Set capError text fill to red
                    capError.setFill(Color.rgb(225,10,10));
                    //Add capError message to TextFlow
                    fileNameTextArea.getChildren().add(capError);
                }
                //else if the newly-typed capacity is both less than enrollment and negative
                else if((newCap < enrollment) && (newCap < 0)) {
                     //Create roomError message for TextFlow
                    Text capacityError = new Text("******Warning****** Enrollment for this course ("  + t.getRowValue().getClassName() + ") is greater than the capacity! \n");
                    //Set roomError text fill to red
                    capacityError.setFill(Color.rgb(225,10,10));
                    //Add roomError message to TextFlow
                    fileNameTextArea.getChildren().add(capacityError);
                    //Create capError message for TextFlow
                    Text capError = new Text("Capacity cannot be less than 0.\n");
                    //Set capError text fill to red
                    capError.setFill(Color.rgb(225,10,10));
                    //Add capError message to TextFlow
                    fileNameTextArea.getChildren().add(capError);
                }
                else {
                    //else, replace the old capacity with the newly-typed capacity
                    t.getRowValue().setCapacity(newCap);
                }
            //refresh the table to show the changes
            this.refreshTable();
        });
        
        enrollmentColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        enrollmentColumn.setOnEditCommit((CellEditEvent<Course, Integer> t) -> {
            //if it's an integer that's typed in
                int newEnrol = t.getNewValue();
                int capacity = t.getRowValue().getCapacity();
                //if capacity is less than the newly-typed enrollment
                if(capacity < newEnrol){
                    //Create roomError message for TextFlow
                    Text enrolError = new Text("******Warning****** Enrollment for this course ("  + t.getRowValue().getClassName() + ") is greater than the capacity! \n");
                    //Set roomError text fill to red
                    enrolError.setFill(Color.rgb(225,10,10));
                    //Add roomError message to TextFlow
                    fileNameTextArea.getChildren().add(enrolError);
                }
                //if the newly-typed enrollment is negative
                else if(newEnrol < 0) {
                    //Create enrollmentError message for TextFlow
                    Text enrollmentError = new Text("Enrollment cannot be less than 0.\n");
                    //Set enrollmentError text fill to red
                    enrollmentError.setFill(Color.rgb(225,10,10));
                    //Add enrollmentError message to TextFlow
                    fileNameTextArea.getChildren().add(enrollmentError);
                }
                else {
                    //else replace enrollment with the newly-typed enrollment
                    t.getRowValue().setExpectedEnrollment(newEnrol);
                }
            //refresh the table to show the changes
            this.refreshTable();
        });

        professorColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        professorColumn.setOnEditCommit((CellEditEvent<Course, String> t) -> {
            
            String fullName = "";
            
            //split the instructor name on any number of whitespace
            String[] names = t.getNewValue().trim().split("\\s+");
            
            for(int i = 0; i < names.length; i++){
                //capitalize first letter of the name
                names[i] = names[i].substring(0, 1).toUpperCase() + names[i].substring(1);
                
                //concat to the fullName string
                if(i == 0){
                    fullName += names[i];
                }
                else{
                    fullName += " " + names[i];
                }
            }
            
            //set the cell value and refresh the table
            t.getRowValue().setProfessorFullName(fullName);
            this.refreshTable();
        });

        timeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        timeColumn.setOnEditCommit((CellEditEvent<Course, String> t) -> {

            //get the old value in the cell to store
            String oldVal = t.getOldValue();
            String newVale = t.getNewValue();

            int number = -1;
            try {
                number =  Integer.parseInt(newVale);
            }
            catch (NumberFormatException e)
            {
                number = -1;
            }
            if(number == -1) { //If the input isn't a number

                boolean isFound = false;

                //check to see if the time period entered is a valid time period
                for (int i = 0; i < ssm.getTimePeriods().size(); i++) {
                    if (t.getNewValue().equalsIgnoreCase(ssm.getTimePeriods().get(i).getTimePeriod())) {
                        isFound = true;
                    }
                }


                //if the time period entered is a valid time period
                if (isFound == true) {

                    //fix the entered data to make it look homogenous
                    String newVal = newVale.toLowerCase();
                    //set the new class time with the (now homogenous) typed class time
                    t.getRowValue().setStringClassTime(newVal);
                }
                //if the time period entered is not a valid time period
                else {
                    //Create timeError message for TextFlow
                    Text timeError = new Text("Invalid Input in Time cell. Invalid time period " + newVale + " entered.\n" +
                                                "\tEnter time in as HH:MMam-HH:MMpm ex: 8:00am-11:20am" +
                                                "\n\t or enter the period #, ex: 1 ");
                    //Set timeError text fill to red
                    timeError.setFill(Color.rgb(225, 10, 10));
                    //Add timeError to TextFlow
                    fileNameTextArea.getChildren().add(timeError);
                    //Reset the value in the cell to the old value
                    t.getRowValue().setStringClassTime(oldVal);
                }
            }
            else { //if input is a period number
                //if the time period entered is a valid time period
                if (number > 0 && number <= ssm.getTimePeriods().size()) {

                    //set the new class time with the (now homogenous) typed class time
                    t.getRowValue().setIntClassTime(number-1);

                }
                //if the time period entered is not a valid time period
                else {
                    //Create timeError message for TextFlow
                    Text timeError = new Text("Invalid Input in Time cell. Invalid time period " + newVale + " entered.\n" +
                                            "\t Time period out of bounds, time periods range from 1 to: " + ssm.getUniversity().getTimePeriods().size());
                    //Set timeError text fill to red
                    timeError.setFill(Color.rgb(225, 10, 10));
                    //Add timeError to TextFlow
                    fileNameTextArea.getChildren().add(timeError);
                    //Reset the value in the cell to the old value
                    t.getRowValue().setStringClassTime(oldVal);
                }
            }
            //refresh the table to show the changes
            this.refreshTable();
        });

        dayColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        dayColumn.setOnEditCommit((CellEditEvent<Course, String> t) -> {
            //This is needed because old value will get changed to the new value after the exception
            String oldVal = t.getOldValue();
            
            try {
                t.getRowValue().setDaysArray(t.getNewValue());
            }
            catch (IllegalArgumentException e) {
                //Create dayError message for TextFlow
                Text dayError = new Text("Invalid Input in Day Cell_old. Valid Values: MW or TR!\n");
                //Set dayError text fill to red
                dayError.setFill(Color.rgb(225,10,10));
                //Add dayError message to TextFlow
                fileNameTextArea.getChildren().add(dayError);
                //Reset the cell value to the old value
                t.getRowValue().setDaysArray(oldVal);
            }
            finally{
               //refresh the table to show the changes
               this.refreshTable();
            }
        });

        roomColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        roomColumn.setOnEditCommit((CellEditEvent<Course, String> t) -> {
            
            String oldVal = t.getOldValue();
            String roomDep = t.getRowValue().getDepartmentName();
            String roomBuilding = t.getRowValue().getBuildingName();
            String building = "";
            boolean flagOne = false;
            boolean flagBoth = false;
            boolean isRoomFound = false;
            
            for(int i = 0; i < ssm.getDepartmentList().size(); i++){
                //find the department that this room belongs to
                if(ssm.getDepartmentList().get(i).getDepartmentName().equals(roomDep)){
                    for(int j = 0; j < ssm.getDepartmentList().get(i).getRoomList().size(); j++){
                        //if it finds the classroom number set boolean to true and get the building name for that room num
                        if(t.getNewValue().equals(ssm.getDepartmentList().get(i).getRoomList().get(j).getRoomNum())){
                            flagOne = true;
                            isRoomFound = true;
                            roomBuilding = ssm.getDepartmentList().get(i).getRoomList().get(j).getBuildingName();
                        }
                        if(t.getNewValue().equals(ssm.getDepartmentList().get(i).getRoomList().get(j).getRoomNum()) && t.getRowValue().getBuildingName().equalsIgnoreCase(ssm.getDepartmentList().get(i).getRoomList().get(j).getBuildingName())){
                            flagBoth = true;
                            isRoomFound = true;
                            roomBuilding = t.getRowValue().getBuildingName();
                            break;
                        }
                    }
                }
            }
            
            //roomnumber found  but in different building of department
            if(flagOne == true && flagBoth == false){
                t.getRowValue().setBuildingName(roomBuilding);
                t.getRowValue().setRoomNum(t.getNewValue());
                for(int i = 0; i < ssm.getUniversity().getBuildingList().size(); i++){
                    if(ssm.getUniversity().getBuildingList().get(i).getBuildingName().equalsIgnoreCase(roomBuilding)){
                        
                        for(int j = 0; j < ssm.getUniversity().getBuildingList().get(i).getRoomList().size(); j++){
                            if(ssm.getUniversity().getBuildingList().get(i).getRoomList().get(j).getRoomNum().equals(t.getNewValue())){
                                t.getRowValue().setCapacity(ssm.getUniversity().getBuildingList().get(i).getRoomList().get(j).getCapacity());
                            }
                        }
                    }
                }
                //if the expected enrollment is larger than the capacity, throw a warning
                if(t.getRowValue().getExpectedEnrollment() > t.getRowValue().getCapacity()){
                    //Create capacityError message for TextFlow
                    Text capacityError = new Text("******Warning****** Enrollment for this course ("  + t.getRowValue().getClassName() + ") is greater than the capacity! \n");
                    //Set capacityError text fill to red
                    capacityError.setFill(Color.rgb(225,10,10));
                    //Add capacityError message to TextFlow
                    fileNameTextArea.getChildren().add(capacityError);
                }
                
                //Create roomError message for TextFlow
                Text roomError = new Text("Room number " + t.getNewValue() + " found, but in different building. Changing to that building \n");
                //Set roomError text fill to red
                roomError.setFill(Color.rgb(225,10,10));
                //Add roomError message to TextFlow
                fileNameTextArea.getChildren().add(roomError);
            }
            //room number found and in same building
            else if(flagOne == true && flagBoth == true){
                t.getRowValue().setRoomNum(t.getNewValue());
                
                for(int i = 0; i < ssm.getUniversity().getBuildingList().size(); i++){
                    if(ssm.getUniversity().getBuildingList().get(i).getBuildingName().equalsIgnoreCase(roomBuilding)){
                        
                        for(int j = 0; j < ssm.getUniversity().getBuildingList().get(i).getRoomList().size(); j++){
                            if(ssm.getUniversity().getBuildingList().get(i).getRoomList().get(j).getRoomNum().equals(t.getNewValue())){
                                t.getRowValue().setCapacity(ssm.getUniversity().getBuildingList().get(i).getRoomList().get(j).getCapacity());
                            }
                        }
                    }
                }
                //if the expected enrollment is larger than the capacity, throw a warning
                if(t.getRowValue().getExpectedEnrollment() > t.getRowValue().getCapacity()){
                    //Create roomError message for TextFlow
                    Text capacityError = new Text("******Warning****** Enrollment for this course ("  + t.getRowValue().getClassName() + ") is greater than the capacity! \n");
                    //Set roomError text fill to red
                    capacityError.setFill(Color.rgb(225,10,10));
                    //Add roomError message to TextFlow
                    fileNameTextArea.getChildren().add(capacityError);
                }
                
                //Create buildingError message for TextFlow
                Text buildingError = new Text("This room (" + t.getNewValue() + ") isn't in the current building. Building also changed\n");
                //Set roomError text fill to red
                buildingError.setFill(Color.rgb(225,10,10));
                //Add buildingError message to TextFlow
                fileNameTextArea.getChildren().add(buildingError);
            }
            //nothing found
            else if (flagOne == false && flagBoth == false){
                fileNameTextArea.setStyle("-fx-text-fill: #E10A0A;");
                Text roomError = new Text("Invalid Input in Room Cell_old. Room number not found in any buildings of this department\n");
                roomError.setFill(Color.rgb(225,10,10));
                fileNameTextArea.getChildren().add(roomError);
            }
            this.refreshTable();
        });


        buildingColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        buildingColumn.setOnEditCommit((CellEditEvent<Course, String> t) -> {
            String oldVal = t.getOldValue();
            String newVal = t.getNewValue().toUpperCase();
            boolean isRoomNumFound = false;
            boolean isBuildingFound = false;
            
            //find the department of this building's row
            for(int i = 0; i < ssm.getDepartmentList().size(); i++){
                //check if it's this department
                if(t.getRowValue().getDepartmentName().equals(ssm.getDepartmentList().get(i).getDepartmentName())){
                    //if it's this department, check for this building
                    for(int j = 0; j < ssm.getDepartmentList().get(i).getBuildingList().size(); j++){
                        //if the building is found
                        if(newVal.equals(ssm.getDepartmentList().get(i).getBuildingList().get(j).getBuildingShortName())){
                            isBuildingFound = true;
                            for(int k = 0; k < ssm.getDepartmentList().get(i).getBuildingList().get(j).getRoomList().size(); k++){
                                //check if the current room number is in this building
                                if(t.getRowValue().getRoomNum().equals(ssm.getDepartmentList().get(i).getBuildingList().get(j).getRoomList().get(k).getRoomNum())){
                                    isRoomNumFound = true;
                                    
                                    //deal with capacity
                                    t.getRowValue().setCapacity(ssm.getUniversity().getBuildingList().get(j).getRoomList().get(k).getCapacity());

                                    if(t.getRowValue().getExpectedEnrollment() > t.getRowValue().getCapacity()){
                                        //Create roomError message for TextFlow
                                        Text capacityError = new Text("******Warning****** Enrollment for this course ("  + t.getRowValue().getClassName() + ") is greater than the capacity! \n");
                                        //Set roomError text fill to red
                                        capacityError.setFill(Color.rgb(225,10,10));
                                        //Add roomError message to TextFlow
                                        fileNameTextArea.getChildren().add(capacityError);
                                    }
                                    
                                    
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            
            //if the building entered is found and the room number is in this building
            if(isRoomNumFound == true){
                //change the building name for this class
                t.getRowValue().setBuildingName(newVal);
            }
            //if building isn't found
            else if(isBuildingFound == false){
                //replace the typed building name with the original
                t.getRowValue().setBuildingName(oldVal);
                //create a buildingError message for the TextFlow
                Text buildingError = new Text("Invalid Input in Building Cell_old. Building name isn't found for this Department. \n");
                //set the buildingError text fill to red
                buildingError.setFill(Color.rgb(225,10,10));
                //add the buildingError to the TextFlow
                fileNameTextArea.getChildren().add(buildingError);
            }
            //building and room do not match
            else{
                //replace the typed building name with the original
                t.getRowValue().setBuildingName(oldVal);
                //create a buildingError message for the TextFlow
                Text buildingError = new Text("Invalid Input in Building Cell_old. Building and Room Number don't match. Change Room Number First and then change the building. \n");
                //set the buildingError text fill to red
                buildingError.setFill(Color.rgb(225,10,10));
                //add the buildingError to the TextFlow
                fileNameTextArea.getChildren().add(buildingError);
            }
            //refresh the table to show the changes
            this.refreshTable();
        });
        
        //the department column cannot be changed by the user
        departmentColumn.setEditable(false);

        //set the widths for each column
        classColumn.setPrefWidth(100);
        professorColumn.setPrefWidth(100);
        capacityColumn.setPrefWidth(100);
        enrollmentColumn.setPrefWidth(100);
        timeColumn.setPrefWidth(100);
        dayColumn.setPrefWidth(100);
        roomColumn.setPrefWidth(100);
        buildingColumn.setPrefWidth(100);
        departmentColumn.setPrefWidth(100);

        //add Style sheets to the columns
        classColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        sectionColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        professorColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        capacityColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        enrollmentColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        timeColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        dayColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        roomColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        buildingColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        departmentColumn.getStyleClass().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        

        //Add the columns to the table
        table.getColumns().addAll(classColumn, sectionColumn, professorColumn, capacityColumn, enrollmentColumn, timeColumn, dayColumn, roomColumn, buildingColumn, departmentColumn);

        //Combo Box For Filtering Departments
        ObservableList<String> comboBoxDepartments
                = FXCollections.observableArrayList(
                "All Departments"
        );

        //TextField to put the file names in once chosen
        fileNameTextArea = new TextFlow();
        //add style sheet to the TextFlow
        fileNameTextArea.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());

        //Create item menus
        Menu menu = new Menu("Upload Files");
        Menu importMenu = new Menu("Import To Table");
        Menu fillTableMenu = new Menu("Auto-Fill Schedule");
        Menu makeScheduleMenu = new Menu("Make Schedule");
        Menu departmentMenu = new Menu("Choose Department");
        Menu resetMenu = new Menu("Reset");

        //add dummy menu items to make the menus clickable
        menu.getItems().add(new MenuItem(""));
        importMenu.getItems().add(new MenuItem(""));
        fillTableMenu.getItems().add(new MenuItem(""));
        makeScheduleMenu.getItems().add(new MenuItem(""));
        resetMenu.getItems().add(new MenuItem(""));

        //this is for the departmentMenu menu (from here until the image adding)
        ToggleGroup toggleGroup = new ToggleGroup();
       
        /*
        Generic event handler for the radio menu items
        This sorts the list based on the selected department in the department menu
        */
        EventHandler<ActionEvent> radioMenuItemHandler = (ActionEvent event) -> {
            RadioMenuItem rmi = (RadioMenuItem) toggleGroup.getSelectedToggle();
            //set it to the full list if all departments selected
            if (rmi.getText().equals("All Departments")) {
                FileLoaderGUI.this.table.setItems(courseList);

            } //go through the list of courses and add them to a new list and display that list
            else {
                //create a temporary list for these courses
                ObservableList<Course> tempCourseList = FXCollections.observableArrayList();
                for (int i = 0; i < courseList.size(); i++) {
                    if (rmi.getText().equals(courseList.get(i).getDepartmentName())) {
                        tempCourseList.add(courseList.get(i));
                    }
                }
                FileLoaderGUI.this.table.setItems(tempCourseList);
            }

            //set the values of each cell in the respective column
            classColumn.setCellValueFactory(new PropertyValueFactory<>("className"));
            sectionColumn.setCellValueFactory(new PropertyValueFactory<>("sectionNumber"));
            professorColumn.setCellValueFactory(new PropertyValueFactory<>("professorFullName"));
            capacityColumn.setCellValueFactory(new PropertyValueFactory<>("capacity"));
            enrollmentColumn.setCellValueFactory(new PropertyValueFactory<>("expectedEnrollment"));
            timeColumn.setCellValueFactory(new PropertyValueFactory<>("stringClassTime"));
            dayColumn.setCellValueFactory(new PropertyValueFactory<>("days"));
            roomColumn.setCellValueFactory(new PropertyValueFactory<>("roomNum"));
            buildingColumn.setCellValueFactory(new PropertyValueFactory<>("buildingName"));
            departmentColumn.setCellValueFactory(new PropertyValueFactory<>("departmentName"));
            event.consume();
        };

        //creates the basic radioMenuItem
        RadioMenuItem allDepItem = new RadioMenuItem("All Departments");
        allDepItem.setText("All Departments");
        allDepItem.setOnAction(radioMenuItemHandler);

        //add to the departmentMenu which acts as the combobox
        departmentMenu.getItems().add(allDepItem);

        //add to the toggle group so it can be shown in the menu
        toggleGroup.getToggles().add(allDepItem);

        //Create images
        ImageView uploadFilesImageView = new ImageView("UploadFiles.png");
        uploadFilesImageView.setFitHeight(20);
        uploadFilesImageView.setFitWidth(20);

        ImageView importToTableImageView = new ImageView("TableImport.png");
        importToTableImageView.setFitHeight(20);
        importToTableImageView.setFitWidth(20);

        ImageView fillTableImageView = new ImageView("FillTable.png");
        fillTableImageView.setFitHeight(20);
        fillTableImageView.setFitWidth(20);

        ImageView makeScheduleImageView = new ImageView("MakeSchedule.png");
        makeScheduleImageView.setFitHeight(20);
        makeScheduleImageView.setFitWidth(20);
        
        ImageView resetImageView = new ImageView("resetIcon.png");
        resetImageView.setFitHeight(20);
        resetImageView.setFitWidth(20);

        //Add images to the menus
        menu.setGraphic(uploadFilesImageView);
        importMenu.setGraphic(importToTableImageView);
        fillTableMenu.setGraphic(fillTableImageView);
        makeScheduleMenu.setGraphic(makeScheduleImageView);
        resetMenu.setGraphic(resetImageView);

        /*
        This is to make it seem like the menu is being clicked.
        This just makes the dummy menu item trigger without showing.
        the menu item on the clicking of the menu.
         */
        int numberOfMenuItems = 1;

        if (numberOfMenuItems == 1) {
            menu.showingProperty().addListener(
                    (observableValue, oldValue, newValue) -> {
                        if (newValue) {
                            // the first menuItem is triggered
                            menu.getItems().get(0).fire();
                        }
                    }
            );
            importMenu.showingProperty().addListener(
                    (observableValue, oldValue, newValue) -> {
                        if (newValue) {
                            // the first menuItem is triggered
                            importMenu.getItems().get(0).fire();
                        }
                    }
            );
            fillTableMenu.showingProperty().addListener(
                    (observableValue, oldValue, newValue) -> {
                        if (newValue) {
                            // the first menuItem is triggered
                            fillTableMenu.getItems().get(0).fire();
                        }
                    }
            );
            makeScheduleMenu.showingProperty().addListener(
                    (observableValue, oldValue, newValue) -> {
                        if (newValue) {
                            // the first menuItem is triggered
                            makeScheduleMenu.getItems().get(0).fire();
                        }
                    }
            );
            resetMenu.showingProperty().addListener(
                    (observableValue, oldValue, newValue) -> {
                        if (newValue) {
                            // the first menuItem is triggered
                            resetMenu.getItems().get(0).fire();
                        }
                    }
            );
        }

        //***************add event listeners to menus******************************
        menu.setOnAction((ActionEvent event) -> {
            //Add a place a choose a file
            filechooser = new FileChooser();
            filechooser.setTitle("Select your CSV files");
            
            //holds the files selected by the user in the fileChooser
            List<File> selectedFiles = filechooser.showOpenMultipleDialog(primaryStage);
            
            //holds the filePaths of the files chosen
            ArrayList<String> filePathList = new ArrayList(20);
            
            if (selectedFiles != null) {
                
                Text selectedText = new Text("Files Selected: \n");
                selectedText.setFill(Color.rgb(0, 0, 0));
                fileNameTextArea.getChildren().add(selectedText);
                
                //Add each selected file to the ArrayList and List
                for (int i = 0; i < selectedFiles.size(); i++) {
                    //boolean for checking if a file has been chosen
                    boolean isAlreadyChosen = false;
                    
                    //checks if the file has been chosen before
                    for(int j = 0; j < ssm.getFilePaths().size(); j++){
                        if(selectedFiles.get(i).getPath().equals(ssm.getFilePaths().get(j))){
                            isAlreadyChosen = true;
                            break;
                        }
                    }
                    
                    if(isAlreadyChosen == false){
                        //get the path of the file
                        filePathList.add(selectedFiles.get(i).getPath());
                        
                        //if new files were added they haven't been imported, so set to false
                        isImported =  false;

                        //print the fileName to the textFlow
                        Text fieldStringText = new Text("    " + selectedFiles.get(i).getName() + "\n");
                        //set the fileName text fill to black
                        fieldStringText.setFill(Color.rgb(0, 0, 0));
                        //add file name to the TextFlow
                        fileNameTextArea.getChildren().add(fieldStringText);
                    }
                    else{
                        //create the warning if a particular file has already been chosen
                        Text fieldStringText = new Text("    " + selectedFiles.get(i).getName() + " has already been selected before. \n");
                        //set the warning text fill to red
                        fieldStringText.setFill(Color.rgb(200, 10, 10));
                        //add the warning to the TextFlow
                        fileNameTextArea.getChildren().add(fieldStringText);
                    }
                }
                ssm.setFilePaths(filePathList);
            }
        });
        importMenu.setOnAction((ActionEvent event) -> {
            try {
                if(isImported == false){
                    //start off by parsing the data files
                    ssm.parseDataFiles();
                    int i;
                    for (i = 0; i < ssm.getDepartmentList().size(); i++) {
                        
                        boolean isFound = false;
                        for(int j = 0; j < departmentMenu.getItems().size(); j++){
                            if(departmentMenu.getItems().get(j).getText().equalsIgnoreCase(ssm.getDepartmentList().get(i).getDepartmentName())){
                                isFound = true;
                            }
                        }
                        if(isFound == false){
                            comboBoxDepartments.add(ssm.getDepartmentList().get(i).getDepartmentName());
                        
                            //for the radio menu item
                            RadioMenuItem rmi = new RadioMenuItem(ssm.getDepartmentList().get(i).getDepartmentName());
                            rmi.setText(ssm.getDepartmentList().get(i).getDepartmentName());
                            rmi.setOnAction(radioMenuItemHandler);
                            departmentMenu.getItems().add(rmi);
                            toggleGroup.getToggles().add(rmi);
                        }
                    }

                    //add the courses to the table
                    this.addCoursesToObservableList();
                    this.table.setItems(courseList);
                    classColumn.setCellValueFactory(new PropertyValueFactory<>("className"));
                    sectionColumn.setCellValueFactory(new PropertyValueFactory<>("sectionNumber"));
                    professorColumn.setCellValueFactory(new PropertyValueFactory<>("professorFullName"));
                    capacityColumn.setCellValueFactory(new PropertyValueFactory<>("capacity"));
                    enrollmentColumn.setCellValueFactory(new PropertyValueFactory<>("expectedEnrollment"));
                    timeColumn.setCellValueFactory(new PropertyValueFactory<>("stringClassTime"));
                    dayColumn.setCellValueFactory(new PropertyValueFactory<>("days"));
                    roomColumn.setCellValueFactory(new PropertyValueFactory<>("roomNum"));
                    buildingColumn.setCellValueFactory(new PropertyValueFactory<>("buildingName"));
                    departmentColumn.setCellValueFactory(new PropertyValueFactory<>("departmentName"));
                    isImported = true;
                }
            } catch (FileParserException f) {
               Text fError = new Text(f.getMessage());
               fError.setFill(Color.rgb(200, 10, 10));
               fileNameTextArea.getChildren().add(fError);
            }
        });

        fillTableMenu.setOnAction((ActionEvent event) -> {
                ssm.populateEmptyCourseData();
                this.addCoursesToObservableList();
                for(int i = 0; i < courseList.size(); i++){
                    for( int j = 0; j < ssm.getUniversity().getRoomList().size(); j++){
                        if(courseList.get(i).getRoomNum().equals(ssm.getUniversity().getRoomList().get(j).getRoomNum())){
                            courseList.get(i).setCapacity(ssm.getUniversity().getRoomList().get(j).getCapacity());
                        }
                    }
                }
                this.table.setItems(courseList);
                //fill each cell in the respective columns
                classColumn.setCellValueFactory(new PropertyValueFactory<>("className"));
                sectionColumn.setCellValueFactory(new PropertyValueFactory<>("sectionNumber"));
                professorColumn.setCellValueFactory(new PropertyValueFactory<>("professorFullName"));
                capacityColumn.setCellValueFactory(new PropertyValueFactory<>("capacity"));
                enrollmentColumn.setCellValueFactory(new PropertyValueFactory<>("expectedEnrollment"));
                timeColumn.setCellValueFactory(new PropertyValueFactory<>("stringClassTime"));
                dayColumn.setCellValueFactory(new PropertyValueFactory<>("days"));
                roomColumn.setCellValueFactory(new PropertyValueFactory<>("roomNum"));
                buildingColumn.setCellValueFactory(new PropertyValueFactory<>("buildingName"));
                departmentColumn.setCellValueFactory(new PropertyValueFactory<>("departmentName"));
        });
        makeScheduleMenu.setOnAction((ActionEvent event) -> {
            if(!scheduleWindowActive) {
                scheduleWindowActive = true;
                Tab swTab = new Tab("Schedule Report");
                sw = new ScheduleWindow(ssm.getUniversity(), ssm.getTimePeriods());
                swTab.setContent(sw);
                tp.getTabs().add(swTab);
                swTab.setOnCloseRequest(new EventHandler<Event>() {
                    @Override
                    public void handle(Event arg0) {

                        scheduleWindowActive = false;

                    }
                });
                swTab.setOnSelectionChanged(new EventHandler<Event>() {
                    @Override
                    public void handle(Event arg0) {
                        if(swTab.isSelected() && scheduleChanged) {
                            sw.refreshTable();
                            scheduleChanged = false;
                        }

                    }
                });
            }
            tp.getSelectionModel().select(1);
        });
        resetMenu.setOnAction((ActionEvent event) -> {
            
            //clear the files chosen
            ssm.clearFilePaths();
            ssm.clearUniversity();
            
            //clear the table
            table.getItems().clear();
            
            //get number of items in this "combo box"
            int size = departmentMenu.getItems().size();
            //removes all but the first items
            departmentMenu.getItems().remove(1, size);
            toggleGroup.getToggles().remove(1, size);
            
            //clear all Text from TextFlow
            fileNameTextArea.getChildren().clear();
            
            //create text to indicate files were cleared
            Text clearText = new Text("All chosen files have been cleared \n");
            //set text fill to black
            clearText.setFill(Color.rgb(0,0, 0));
            //add the text to the TextFlow
            fileNameTextArea.getChildren().add(clearText);
        });
        
        //Make a menu bar and add the menus to it
        MenuBar menuBar = new MenuBar();
        menuBar.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        menuBar.getMenus().add(resetMenu);
        menuBar.getMenus().add(menu);
        menuBar.getMenus().add(importMenu);
        menuBar.getMenus().add(fillTableMenu);
        menuBar.getMenus().add(makeScheduleMenu);
        menuBar.getMenus().add(departmentMenu);
        menuBar.prefWidthProperty().bind(primaryStage.widthProperty());

        //panes for the gui
        BorderPane borderPane = new BorderPane();
        HBox centerPane = new HBox();
        centerPane.prefWidthProperty().bind(borderPane.widthProperty());
        centerPane.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        HBox hbox = new HBox();
        hbox.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        hbox.prefHeightProperty().bind(menuBar.heightProperty());
        hbox.prefWidth(850);
        HBox bottomHbox = new HBox();
        bottomHbox.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        bottomHbox.prefWidthProperty().bind(borderPane.widthProperty());
        bottomHbox.setMinHeight(100);


        //autoresizing
        table.prefWidthProperty().bind(borderPane.widthProperty());
        table.prefHeightProperty().bind(centerPane.heightProperty());

        classColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        classColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        sectionColumn.minWidthProperty().bind(table.widthProperty().divide(18.0));
        sectionColumn.prefWidthProperty().bind(table.widthProperty().divide(18.0));
        professorColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        professorColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        capacityColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        capacityColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        enrollmentColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        enrollmentColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        timeColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        timeColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        dayColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        dayColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        roomColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        roomColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        buildingColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        buildingColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));
        departmentColumn.minWidthProperty().bind(table.widthProperty().divide(9.0));
        departmentColumn.prefWidthProperty().bind(table.widthProperty().divide(9.0));

        fileNameTextArea.prefHeightProperty().bind(bottomHbox.heightProperty());
        fileNameTextArea.prefWidthProperty().bind(borderPane.widthProperty());
        
        //Create scrollpane for fileNameTextArea
        ScrollPane sp_fileNameTextArea = new ScrollPane(fileNameTextArea);
        sp_fileNameTextArea.prefHeightProperty().bind(bottomHbox.heightProperty());
        sp_fileNameTextArea.prefWidthProperty().bind(borderPane.widthProperty());
        //disable Hbar (we don't need to scroll to the left or right)
        sp_fileNameTextArea.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        //set the beginning focus for the scrollpane to the top
        sp_fileNameTextArea.setVvalue(0.0);
        
        //Make sure ScrollPane stays at the latest line
        fileNameTextArea.heightProperty().addListener(observable -> sp_fileNameTextArea.setVvalue(1D));
        
        borderPane.setCenter(centerPane);
        borderPane.setTop(hbox);
        borderPane.setBottom(bottomHbox);
        hbox.getChildren().add(menuBar);
        centerPane.getChildren().add(table);
        bottomHbox.getChildren().add(sp_fileNameTextArea);
        Tab mainTab = new Tab("Data Entry");
        mainTab.setContent(borderPane);
        mainTab.setClosable(false);

        tp.getTabs().add(mainTab);

        Scene scene = new Scene(tp, 1000, 800);
        scene.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        primaryStage.setTitle("Course Scheduler");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    private void addCoursesToObservableList() {
        //first clear the arrayList
        this.courseList.clear();
        //now add the courses to start fresh again
        SchedulingSystemModel ssm = SchedulingSystemModel.getInstance();
        for (int i = 0; i < ssm.getDepartmentList().size(); i++) {
            for (int j = 0; j < ssm.getDepartmentList().get(i).getSizeofCourseList(); j++) {
                this.courseList.add(ssm.getDepartmentList().get(i).getCourseList().get(j));
            }
        }
    }
   
    private void refreshTable(){
        table.refresh();
        scheduleChanged = true;
    }

    /**
     * @param args the command line arguments
     */
    public void FileLoaderGUIStart(String[] args) {
        launch(args);
    }
    
    private FileChooser filechooser;
    private TableView<Course> table;
    private SchedulingSystemModel ssm;
    private TextFlow fileNameTextArea;
    private boolean scheduleWindowActive = false;
    private boolean scheduleChanged = false;
    private ObservableList<Course> courseList = FXCollections.observableArrayList();
    private ScheduleWindow sw;

    //things for the menu
    private boolean isImported = false;
    
}

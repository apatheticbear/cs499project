package CourseScheduler.GUI;

//Class to represent an individual cell within a ScheduleGrid_old

import CourseScheduler.MVC.SchedulingSystemModel;
import CourseScheduler.UniversityParts.Instructor;
import CourseScheduler.UniversityParts.Room;
import java.util.ArrayList;

public class Cell
{
    private int data;
    private boolean hasConflict;
    private ArrayList<Room> rooms;
    private ArrayList<Boolean> isRoomAssigned;
    private int row;
    private int col;
    private int openRoomIndex;
    private ArrayList<Integer> instructorIndex;
    private ArrayList<Integer> courseIndex;

    Cell(ArrayList<Room> rooms)
    {
        this.data = 0;
        this.hasConflict = false;
        this.rooms = rooms;
    }

    //used for creating scheduleGrid. Cell_old with no real data
    Cell()
    {
        this.data = 0;
        this.hasConflict = false;
        this.rooms = new ArrayList<>();
        this.isRoomAssigned = new ArrayList<>();
        this.instructorIndex = new ArrayList<>();
    }
    
    //***************Mine **********************
    
    //initialize all booleans to false
    public void initBooleans(){
        for(int i = 0; i < this.rooms.size(); i++){
            this.isRoomAssigned.add(false);
        }
    }
    
    //sets the boolean at a given room index to the specified value
    public void setRoomIsAssigned(int roomIndex, boolean isAssigned){
        this.isRoomAssigned.set(roomIndex, isAssigned);
    }
    
    //returns the boolean correlated to roomIndex
    public boolean getIsAssignedAtRoomIndex(int roomIndex){  
        return this.isRoomAssigned.get(roomIndex);
    }
    
    //stores the professor index in the spot corresponding to the roomNum
    public void addInstructorToCell(int row, int column, int roomNum, int profNumber){
        this.instructorIndex.set(roomNum, profNumber);
    }
    
    //given a roomNum, get the instructor at the corresponding index in the instructorIndexList
    public int getInstructorAtRoomIndex(int roomNum){
        return this.instructorIndex.get(roomNum);
    }
    
    public Instructor getInstructorAtIndex(int index){
        SchedulingSystemModel ssm = SchedulingSystemModel.getInstance();
        return ssm.getUniversity().getInstructorList().get(index);
    }
    
    
    //***********Jakes************************

    public void initRooms(ArrayList<Room> inputRooms)
    {
        rooms = inputRooms;
    }

    public void triggerConflict()
    {
        this.hasConflict = true;
    }

    public void resolveConflict()
    {
        this.hasConflict = false;
    }

    public void printCellData()
    {
        System.out.println("Rooms: ");

        for(int i = 0; i < this.rooms.size(); i++)
        {
            //System.out.println(this.rooms.get(i).getRoomInfo());
            this.rooms.get(i).printFullRoomData();
        }

        System.out.println("---------------\n");
    }

    public Room getRoomByIndex(int index)
    {
        return rooms.get(index);
    }

    public void checkCellForConflict()
    {
        for(int i = 0; i < this.rooms.size(); i++)
        {
            if(this.rooms.get(i).getNumCoursesInRoom() > 1)
            {
                System.out.println("Conflict detected in room: " + this.rooms.get(i).getRoomInfo());
                System.out.println("Conflicting classes: ");

                //Print the courses which are causing a conflict
                for(int j = 0; j < this.rooms.get(j).getCourseList().size(); j++)
                {
                    // System.out.print(this.rooms.get(j).getAllCoursesInRoom().get(j).getFullName() + " ");
                }

                System.out.println();
                this.hasConflict = true;
            }
        }
    }

    public void addRoom(Room newRoom)
    {
        this.rooms.add(newRoom);
        this.isRoomAssigned.add(false);
        this.instructorIndex.add(-1);
    }

    public ArrayList<Room> getRooms()
    {
        return this.rooms;
    }

    public Room getAnAvailableRoom()
    {
        for(int i = 0; i < this.getRooms().size(); i++)
        {
            if(this.getRoomByIndex(i).getAvailability() == true)
            {
                return this.getRoomByIndex(i);
            }
        }

        return null;
    }

    public void setRow(int row)
    {
        this.row = row;
    }

    public void setCol(int col)
    {
        this.col = col;
    }

    public int getRow()
    {
        return this.row;
    }

    public int getCol()
    {
        return this.col;
    }

    public void setOpenRoomIndex(int roomIndex)
    {
        this.openRoomIndex = roomIndex;
    }

    public int getOpenRoomIndex()
    {
        return openRoomIndex;
    }
}

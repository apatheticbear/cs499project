package CourseScheduler.GUI;

/*
 * Written by Tristan Davis
 * This class represents a row in the ScheduleGantt table
 * It maintains a 2d array of scrolls panes containing buttons, each button being the display of a course.
 */
import CourseScheduler.UniversityParts.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class RowRoom {
    private String roomName;
    private Button roomNameButton;
    private ScrollPane[][] week;
    private String displayOption;
    private boolean isHighlighted;
    private int periodCount;
    private PseudoClass highlightScrollpane = PseudoClass.getPseudoClass("highlight");
    private ScheduleGantt sg;

    RowRoom(int periodCount, Room room, String option, ScheduleGantt newSG){
        sg = newSG;
        displayOption = option;
        this.periodCount = periodCount;
        roomName = room.getRoomInfo();
        roomNameButton = new Button(roomName);
        ArrayList<String> courseNameList = new ArrayList<String>();
        for(Course c : room.getCourseList()) {
            courseNameList.add(c.getClassAndSection());
        }
        //Set button event
        roomNameButton.setOnAction((ActionEvent event) -> {

            String[] properties = {"Class", "Time", "Day"};
            TableView tv = new TableView();
            VBox vbox = new VBox();
            ObservableList obs_course;
            // columns = new TableColumn[properties.length];
            TableColumn columnClass = new TableColumn(properties[0]);
            columnClass.setCellValueFactory(new PropertyValueFactory<Course, String>("classAndSection"));
            TableColumn columnTime = new TableColumn(properties[1]);
            columnTime.setCellValueFactory(new PropertyValueFactory<Course, String>("stringClassTime"));
            TableColumn columnDay = new TableColumn(properties[2]);
            columnDay.setCellValueFactory(new PropertyValueFactory<Course, String>("days"));

            obs_course =  FXCollections.observableList(room.getCourseList());

            // Insert columns into tables
            tv.getColumns().addAll(columnDay, columnTime, columnClass);
            tv.setItems(obs_course);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            tv.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());

            alert.setTitle("Room Information Dialog");
            alert.setHeaderText("Room " + room.getRoomInfo() + " Detail\n\t Capacity: " + room.getCapacity() );

            alert.getDialogPane().setContent(tv);
            alert.getDialogPane().setPrefSize(300,400);

            alert.showAndWait();
        });

        //Initialize the week matrix with scroll panes
        week = new ScrollPane[7][periodCount];
        for(int i = 0; i < 7; i ++) {
            for(int j = 0; j < periodCount; j ++){
                VBox box = new VBox();
                box.setSpacing(4);
                //box.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
                week[i][j] = new ScrollPane();
                week[i][j].getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
                //week[i][j].setPrefSize(85,50);
                week[i][j].pseudoClassStateChanged(highlightScrollpane, false);
                week[i][j].setContent(box);
            }
        }
    }

    //Set the time period
    String getPeriod(String day, int period){
        String value;
        Node n;
        switch (day) {
            case "Monday":
                value = "";
                n = week[0][period].getContent();
                if(n != null) {
                    VBox box = (VBox) n; //Conversion could throw error
                    ObservableList<Node> buttons = box.getChildren();
                    if(buttons.size() > 0) {
                        Button b = (Button) buttons.get(0);
                        value = b.getText();
                    }
                }
                return value;

            case "Tuesday":
                value = "";
                n = week[1][period].getContent();
                if(n != null) {
                    VBox box = (VBox) n; //Conversion could throw error
                    ObservableList<Node> buttons = box.getChildren();
                    if(buttons.size() > 0) {
                        Button b = (Button) buttons.get(0);
                        value = b.getText();
                    }
                }
                return value;
            case "Wednesday":
                value = "";
                n = week[2][period].getContent();
                if(n != null) {
                    VBox box = (VBox) n; //Conversion could throw error
                    ObservableList<Node> buttons = box.getChildren();
                    if(buttons.size() > 0) {
                        Button b = (Button) buttons.get(0);
                        value = b.getText();
                    }
                }
                return value;
            case "Thursday":
                value = "";
                n = week[3][period].getContent();
                if(n != null) {
                    VBox box = (VBox) n; //Conversion could throw error
                    ObservableList<Node> buttons = box.getChildren();
                    if(buttons.size() > 0) {
                        Button b = (Button) buttons.get(0);
                        value = b.getText();
                    }
                }
                return value;
            case "Friday":
                value = "";
                n = week[4][period].getContent();
                if(n != null) {
                    VBox box = (VBox) n; //Conversion could throw error
                    ObservableList<Node> buttons = box.getChildren();
                    if(buttons.size() > 0) {
                        Button b = (Button) buttons.get(0);
                        value = b.getText();
                    }
                }
                return value;
            default:
                System.out.println("Error adding class to time period: invalid day");
                //Delete temp button?
                return null;
        }
    }

    //Get the period button with a given day and period
    ScrollPane getPeriodButton(String day, int period){
        switch (day) {
            case "Monday":
                return week[0][period];

            case "Tuesday":
                return week[1][period];

            case "Wednesday":
                return week[2][period];
            case "Thursday":
                return week[3][period];

            case "Friday":
                return week[4][period];

            default:
                System.out.println("Error adding class to time period: invalid day");
                return null;
        }
    }

    //Add a class to the row
    void addClass(Course newCourse) {
        ArrayList<Day.day> theDays = newCourse.getDaysArray();
        TimePeriod theTime = newCourse.getTimePeriod();

        if(theDays != null && theTime != null) { //Make sure the day and time is initialized
            int time = theTime.getPeriodNumber(); //Time period variable
            for(Day.day aDay : theDays) { //Iterate through days
                // System.out.println(time);
                Button button = new Button();

                //Determine if the button text will be the course or the professor
                if(displayOption.equals("Course")) {
                    button.setText(newCourse.getClassAndSection());
                    button.setOnAction((ActionEvent event) -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);

                        alert.setTitle("Course Information Dialog");
                        alert.setHeaderText(newCourse.getClassAndSection() + " Detail");
                        alert.getDialogPane().setContent(new CourseInfoPopup(newCourse));

                        alert.showAndWait();});
                }
                else if( displayOption.equals("Professor")) {
                    button.setText(newCourse.getProfessorFullName());
                    button.setOnAction((ActionEvent event) -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);

                        alert.setTitle("Course Information Dialog");
                        alert.setHeaderText(newCourse.getClassAndSection() + " Detail");
                        alert.getDialogPane().setContent(new CourseInfoPopup(newCourse));

                        alert.showAndWait();
                    });

                }

                //Check and see if there is a conflict
                VBox box = (VBox) week[aDay.ordinal()][time].getContent();
                if(!this.isHighlighted() && newCourse.getConflict()) {
                    //If there is a conflict set the conflict style
                    week[aDay.ordinal()][time].pseudoClassStateChanged(highlightScrollpane, true);
                    //week[aDay.ordinal()][time].setPrefSize(85,75);
                    highlightRow(true);
                }
                box.getChildren().add(button);
            }
        }
    }

    public Button getRoomNameButton() {
        return roomNameButton;
    }

    String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
        this.roomNameButton.setText(roomName);
    }

    //Look for conflicts
    public void findConflicts() {
        for(int i = 0; i < 7; i++) //7 days in a week
        {
            for(int j = 0; j < periodCount; j++) { //time periods in a day
               ScrollPane tempPane =  week[i][j];
               VBox aBox = (VBox) tempPane.getContent();
               List<Node> boxChildrenList = aBox.getChildren();
               if(boxChildrenList.size() > 1) { //If theres more than one button
                   highlightRow(true);
                   for(int k = 0; k < boxChildrenList.size(); k++) {
                       if(boxChildrenList.get(k).getClass().toGenericString().equals("public class javafx.scene.control.Button")) {
                           Button b = (Button) boxChildrenList.get(k);
                           sg.setConflicts(b.getText());
                       }
                   }
               }
               else {
                   if(boxChildrenList.size() > 0 && this.isHighlighted()) {
                       Button b = (Button) boxChildrenList.get(0);
                       highlightRow(false);
                       sg.clearConflicts(b.getText());
                   }
               }
            }
        }
    }

    //Function to apply highlight style to the row
    public void highlightRow(boolean highlight) {
        if(!this.isHighlighted) { //If this row isn't highlighted yet we need to go back and highlight other scrollpanes
            for (int i = 0; i < 7; i++) {
                for (int j = 0; j < periodCount; j++) {
                    week[i][j].pseudoClassStateChanged(highlightScrollpane, true);
                }
            }
        }
        if(highlight) {
            this.isHighlighted = true;
        }
        else {
            this.isHighlighted = false;
        }
    }

    //Variable for managing the highlighted state of the row
    public boolean isHighlighted() {
        return isHighlighted;
    }


}

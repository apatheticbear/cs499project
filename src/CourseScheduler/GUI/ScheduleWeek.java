package CourseScheduler.GUI;

import CourseScheduler.UniversityParts.*;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import java.util.ArrayList;

/*
    Written by  Tristan Davis
        This class creates a table of a weekly schedule for a specified room or professor.
 */

public class ScheduleWeek extends HBox {
    private ArrayList<RowIndividual> courses;
    private ArrayList<Course> tableCourses;
    private PseudoClass highlightRow = PseudoClass.getPseudoClass("highlight");
    private TableView<RowIndividual> scheduleTable;

    //Create schedule for instructor
    ScheduleWeek(Instructor instructor, ArrayList<TimePeriod> times) {
        courses = new ArrayList<>();
        tableCourses = instructor.getCourseList();
        //Initialize the table data
        for (TimePeriod time : times) {

            courses.add(new RowIndividual(time.getTimePeriod(), this));
        }
        //Iterate through instructor courses to add classes to columns
        for(int i = 0; i < instructor.getCourseList().size(); i ++){

            TimePeriod classTime = instructor.getCourseList().get(i).getTimePeriod();
            if(classTime != null) {
                ArrayList<Day.day> classDay = instructor.getCourseList().get(i).getDaysArray();
                courses.get(classTime.getPeriodNumber()).addClass(instructor.getCourseList().get(i));
            }
        }
        // Create observable list with courses data
        ObservableList observableCourseList = FXCollections.observableArrayList(courses);
        //Create the table with formatted data
        makeTable(observableCourseList, times);
    }

    //Create schedule for room
    ScheduleWeek(Room room, ArrayList<TimePeriod> times) {
        courses = new ArrayList<>();
        //Initialize the table data
        for (TimePeriod time : times) {
            courses.add(new RowIndividual(time.getTimePeriod(), this));
        }
        tableCourses = room.getCourseList();
        // iterate through courses finding all classes within them and assign them to columns
        for(int i = 0; i < room.getCourseList().size(); i ++){
            TimePeriod classTime = room.getCourseList().get(i).getTimePeriod();
            ArrayList<Day.day> classDay =  room.getCourseList().get(i).getDaysArray();
            courses.get(classTime.getPeriodNumber()).addClass(room.getCourseList().get(i));
        }

        //Create the table with formatted data
        ObservableList observableCourseList = FXCollections.observableArrayList(courses);
        makeTable(observableCourseList, times);
    }

    private void makeTable(ObservableList<RowIndividual> observableIndividualList, ArrayList<TimePeriod> times) {
        scheduleTable = new TableView<>();

        //Initialize columns of the schedule table
        scheduleTable.getColumns().add(periodColumn());

        scheduleTable.getColumns().add(dayColumn("Monday"));
        scheduleTable.getColumns().add(dayColumn("Tuesday"));
        scheduleTable.getColumns().add(dayColumn("Wednesday"));
        scheduleTable.getColumns().add(dayColumn("Thursday"));
        scheduleTable.getColumns().add(dayColumn("Friday"));

        //Initialize course table
        for (TimePeriod time : times) {
            courses.add(new RowIndividual(time.getTimePeriod(), this));
        }
        scheduleTable.setItems(observableIndividualList);

        //Highlight rows which have a conflict.
        scheduleTable.setRowFactory(tv -> new TableRow<RowIndividual>() {


            @Override
            public void updateItem(RowIndividual item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    pseudoClassStateChanged(highlightRow, false);
                } else {
                    updateState(this);
                }
            }
        });

        VBox vbox = new VBox();
        vbox.getChildren().add(scheduleTable);

        //Bind width and height of the table to the vbox
        scheduleTable.prefWidthProperty().bind(vbox.widthProperty());
        scheduleTable.prefHeightProperty().bind(vbox.heightProperty());

        //bind the vbox to the hbox (this, ScheduleWeek)
        vbox.prefWidthProperty().bind(this.widthProperty());
        vbox.prefHeightProperty().bind(this.heightProperty());

        this.getChildren().add(vbox);
    }


    //Create the room column, this column displays the room names
    private TableColumn periodColumn(){
        TableColumn column_period = new TableColumn<RowIndividual, String>("Period");
        getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        column_period.setCellValueFactory(new PropertyValueFactory<RowIndividual, String>("period"));

        return column_period;

    }

    private TableColumn dayColumn(String day ){
        // Create day column
        TableColumn column_day = new TableColumn<RowIndividual, String>(day);
       // column_day.setPrefWidth(100);
        getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        column_day.setCellValueFactory((Callback<TableColumn.CellDataFeatures<RowIndividual, String>, ObservableValue<Button>>) p -> {

            return new ReadOnlyObjectWrapper(p.getValue().getClass(day));
        });
        column_day.setSortable(false);
        return column_day;
    }

    //Update the state of the row; this highlights the row
    private <T> void updateState(TableRow<T> row) {
        TableView<T> table = row.getTableView() ;
        T item = row.getItem();
        RowIndividual r = (RowIndividual) item;
        r.findConflicts();
        // if item is selected, just use default "selected" highlight,
        // and set "child-of-selected" and "parent-of-selected" to false:
        if (item == null ) {
            row.pseudoClassStateChanged(highlightRow, false);
            // row.pseudoClassStateChanged(parentOfSelected, false);
            return ;
        }

        //System.out.println(c.isHighlighted());
        if (r.isHighlighted()) {
            row.pseudoClassStateChanged(highlightRow, true);
            // row.pseudoClassStateChanged(childOfSelected, false);
            return ;
        }

        // if we got this far, clear both pseudoclasses:
        row.pseudoClassStateChanged(highlightRow, false);
    }

    //set conflicts in courses where conflicts are detected
    public void setConflicts(String courseInfo) {
        boolean courseFound = false;
        int courseIndex = 0;
        while(!courseFound && courseIndex < tableCourses.size()) {
            if(courseInfo.equals(tableCourses.get(courseIndex).getClassAndSection())) {
                tableCourses.get(courseIndex).setConflict(true);
                courseFound = true;
            }
            courseIndex++;
        }
    }

    public void clearConflicts(String courseInfo) {
        boolean courseFound = false;
        int courseIndex = 0;
        while(!courseFound && courseIndex < tableCourses.size()) {
            if(courseInfo.equals(tableCourses.get(courseIndex).getClassAndSection())) {
                tableCourses.get(courseIndex).setConflict(false);
                courseFound = true;
            }
            courseIndex++;
        }
    }}

package CourseScheduler.GUI;

/*
 * Written by Tristan Davis
 * This is like a gantt chart but not quite. Its implemented as a table.
 * */

import CourseScheduler.UniversityParts.Course;
import CourseScheduler.UniversityParts.Building;
import CourseScheduler.UniversityParts.Room;
import CourseScheduler.UniversityParts.TimePeriod;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.Comparator;


public class ScheduleGantt extends HBox {
    private PseudoClass highlightRow = PseudoClass.getPseudoClass("highlight");
    private TableView<RowRoom> ganttTable;
    private ObservableList obs_rooms; //An observable list for the table to update contents
    private ArrayList<TimePeriod> tableTimes;
    private ArrayList<Course> tableCourses;
    private ArrayList<Building> tableBuildings;
    private String tableOption;


    ScheduleGantt(ArrayList<TimePeriod> times, ArrayList<Course> courses, ArrayList<Building> buildings, String displayOption){

        tableTimes = times;
        tableCourses = courses;
        tableBuildings = buildings;
        tableOption = displayOption;

        ganttTable = new TableView<>();
        ArrayList<RowRoom> rooms = new ArrayList<>();
        VBox vbox; //Vertical box container for the table
        ArrayList<TableColumn> columns = new ArrayList<TableColumn>();

        columns.add(roomColumn());
        ganttTable.getColumns().add(columns.get(0));

        columns.add(dayColumn(times, "Monday"));
        columns.add(dayColumn(times, "Tuesday"));
        columns.add(dayColumn(times, "Wednesday"));
        columns.add(dayColumn(times, "Thursday"));
        columns.add(dayColumn(times, "Friday"));

        ganttTable.getColumns().add(columns.get(1));
        ganttTable.getColumns().add(columns.get(2));
        ganttTable.getColumns().add(columns.get(3));
        ganttTable.getColumns().add(columns.get(4));
        ganttTable.getColumns().add(columns.get(5));

        //Initialize the rooms array, which is essentially the rows of the table
        for (Building building : buildings) {
            //Create a new room

            ArrayList<Room> buildingRooms = building.getRoomList();
            // Search through room list
            for (Room buildingRoom : buildingRooms) {
                rooms.add(
                        new RowRoom(times.size(), buildingRoom, displayOption, this)
                );
            }
        }

        //Fill in the room data based on courses
        for (Course course : courses) {
            int j = 0;
            boolean roomFound = false;
            while ((j < rooms.size()) && (!roomFound)) {
                if (course.getRoomInfo().equals(rooms.get(j).getRoomName())) {
                    rooms.get(j).addClass(course);
                    roomFound = true;
                } else if (j >= rooms.size()) {
                    System.out.println("ScheduleGantt.java: Unable to find building: " + course.getBuildingName() + "For class: " +
                            course.getClassAndSection());
                }
                j++;
            }
        }

        obs_rooms = FXCollections.observableList(rooms);


        //Set contents of gantt table
        ganttTable.setItems(obs_rooms);
        //Set visibility of ganttTable menu button
        ganttTable.setTableMenuButtonVisible(true);



        //Highlight rows which have a conflict.
        ganttTable.setRowFactory(tv -> new TableRow<RowRoom>() {


            @Override
            public void updateItem(RowRoom item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    pseudoClassStateChanged(highlightRow, false);
                } else {
                    updateState(this);
                }
            }
        });


        vbox = new VBox();
        vbox.getChildren().add(ganttTable);
        //Bind width and height of the table to the vbox
        ganttTable.prefWidthProperty().bind(vbox.widthProperty());
        ganttTable.prefHeightProperty().bind(vbox.heightProperty());

        //Bind the vbox to the hbox (this, ScheduleGantt)
        vbox.prefWidthProperty().bind(this.widthProperty());
        vbox.prefHeightProperty().bind(this.heightProperty());

        this.getChildren().add(vbox);
    }

    //Create day columns which have period sub columns
    private TableColumn dayColumn(ArrayList<TimePeriod> times, String day){
        // Create day column
        TableColumn column_day = new TableColumn<RowRoom, ScrollPane>(day);
        column_day.getStyleClass().add("major");

        column_day.setSortable(false);
        // Create period columns
        for(int i = 0; i < times.size(); i++) {

            TableColumn column_period = new TableColumn<RowRoom, String>(times.get(i).getTimePeriod());

            column_period.setSortable(false);

            if(i == 0) {
                column_period.getStyleClass().add("minorEdge");
            }

            column_period.setId(Integer.toString(i));
            column_day.getColumns().add(column_period);
            String id = column_period.getId();

            //Cell_old value factory
            column_period.setCellValueFactory((Callback<TableColumn.CellDataFeatures<RowRoom, ScrollPane>, ObservableValue<Button>>) p -> {

                String theId = p.getTableColumn().getId();
                String theDay = p.getTableColumn().getParentColumn().getText();
                //If the button has no text return null, so as not to have empty buttons on the table
                if(p.getValue().getPeriod(theDay, Integer.parseInt(theId)).equals("")) {
                    return null;
                }

                ReadOnlyObjectWrapper value = new ReadOnlyObjectWrapper(p.getValue().getPeriodButton(theDay, Integer.parseInt(theId)));
                return value;


            });

        }

        return column_day;
    }

    //Create the room column, this column displays the room names
    private TableColumn roomColumn(){
        TableColumn column_room = new TableColumn<RowRoom, String>("Room");

        column_room.setCellValueFactory(new PropertyValueFactory<RowRoom, Button>("roomNameButton"));
        column_room.setComparator(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                //Get button 1
                Button button1 = (Button) o1;

                //Get button 2
                Button button2 = (Button) o2;

                String stringOne = button1.getText();
                String stringTwo = button2.getText();
                return stringOne.compareTo(stringTwo);
            }
        });

        return column_room;

    }

    private <T> void updateState(TableRow<T> row) {
        TableView<T> table = row.getTableView() ;
        T item = row.getItem();
        RowRoom c = (RowRoom) item;
        c.findConflicts();
        // if item is selected, just use default "selected" highlight,
        // and set "child-of-selected" and "parent-of-selected" to false:
        if (item == null ) {
            row.pseudoClassStateChanged(highlightRow, false);
            // row.pseudoClassStateChanged(parentOfSelected, false);
            return ;
        }

        //System.out.println(c.isHighlighted());
        if (c.isHighlighted()) {
            row.pseudoClassStateChanged(highlightRow, true);
            // row.pseudoClassStateChanged(childOfSelected, false);
            return ;
        }

        // if we got this far, clear both pseudoclasses:
        row.pseudoClassStateChanged(highlightRow, false);

    }

    //set conflicts in courses where conflicts are detected
    public void setConflicts(String courseInfo) {
        boolean courseFound = false;
        int courseIndex = 0;
        while(!courseFound && courseIndex < tableCourses.size()) {
            if(courseInfo.equals(tableCourses.get(courseIndex).getClassAndSection())) {
                tableCourses.get(courseIndex).setConflict(true);
                courseFound = true;
            }
            courseIndex++;
        }
    }

    public void clearConflicts(String courseInfo) {
        boolean courseFound = false;
        int courseIndex = 0;
        while(!courseFound && courseIndex < tableCourses.size()) {
            if(courseInfo.equals(tableCourses.get(courseIndex).getClassAndSection())) {
                tableCourses.get(courseIndex).setConflict(false);
                courseFound = true;
            }
            courseIndex++;
        }
    }
}



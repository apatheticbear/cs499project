package CourseScheduler.GUI;

/*
  Written by Tristan Davis
  This window will be able to display a schedule based on department, classroom, and professor (11)
    Modifications by Hayley Johnsey (chooseDirectory and reportDonePopup)
 */

import CourseScheduler.UniversityParts.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.DirectoryChooser;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class ScheduleWindow extends BorderPane {
    private ScheduleGantt scheduleGantt;
    private ScheduleWeek scheduleWeek;
    private ScheduleList scheduleList;
    private ScheduleMenuBar menuBar;
    private Stage myStage;
    private Scene tableScene;
    private ArrayList<TimePeriod> times;
    private ArrayList<Department> departments;
    private University university;

    ScheduleWindow(University uni, ArrayList<TimePeriod> times){ // Parameters should include the schedule data
        this.times = times;
        menuBar = new ScheduleMenuBar();
        departments = uni.getDepts();
        university = uni;

        //Declare list for schedule by dept/prof/room combo box
        ArrayList<String> scheduleByList = new ArrayList<>();
        ObservableList<String> observableScheduleByList;

        //Declare list for department combobox
        ArrayList<String> deptNames = new ArrayList<>();
        ObservableList<String> comboBoxDepartments;

        //Declare list for professor/room combobox
        ArrayList<String> individuals = new ArrayList<>();
        ObservableList<String> comboBoxIndividual;

        //Declare list of schedule types
        ArrayList<String> ScheduleStyles = new ArrayList();
        ObservableList<String> observableScheduleStyles;

        ScheduleStyles.add("Chart");
        ScheduleStyles.add("List");

        observableScheduleStyles = FXCollections.observableArrayList(ScheduleStyles);

        menuBar.setCb_scheduleStyle(new ComboBox(observableScheduleStyles));


        //Create a combobox for setting the style
        menuBar.getCb_scheduleStyle().getSelectionModel().selectedItemProperty().addListener( (options, oldValue, newValue) -> {

            if(!oldValue.equals(newValue)) {//If the style changes it is necessary to create a new table

                //Gather selection data from the other combo boxes
                String scheduleGroup = menuBar.getCb_scheduleGroup().getSelectionModel().getSelectedItem().toString();
                int selectedDept = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex() - 1;
                int selectedRoom = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex();
                int selectedIndividualIndex = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex();
                String selectedIndividual = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedItem().toString();
                String selectedScheduleStyle = menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString();

                switch(scheduleGroup){ //Determine which type of table will be created
                    case "Department":

                        //Department dept = departments.get(selectedDept);
                        createDepartmentTable(selectedDept, selectedIndividual, selectedScheduleStyle);
                        break;
                    case "Room":
                        //Room rm = departments.get(selectedDept).getRoomList().get(selectedRoom);
                        createRoomTable(selectedDept, selectedRoom ,selectedScheduleStyle );
                        break;
                    case "Professor":
                        //Instructor prof = departments.get(selectedDept).getInstructor(selectedIndividualIndex);
                        createProfessorTable(selectedDept, selectedIndividualIndex, selectedScheduleStyle);
                        break;
                }
                setScheduleSettings(menuBar.getCb_scheduleGroup().getSelectionModel().getSelectedItem().toString(),
                        newValue.toString(),
                        menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1,
                        times);
                menuBar.requestFocus();
                menuBar.getCb_scheduleStyle().requestFocus();
            }
        });


        //Add options to the schedule by list
        scheduleByList.add("Department");
        scheduleByList.add("Room");
        scheduleByList.add("Professor");

        observableScheduleByList = FXCollections.observableArrayList(scheduleByList);

        //Create a combobox for setting the group whose data will be represented
        menuBar.setCb_scheduleGroup(new ComboBox(observableScheduleByList));
        menuBar.getCb_scheduleGroup().getSelectionModel().selectedItemProperty().addListener( (options, oldValue, newValue) -> {
                    //Remove any old tables
                    if(!oldValue.equals(newValue)) { //If the  selection is new remove old table first
                        if (oldValue.equals("Department")) {
                            this.getChildren().remove(scheduleGantt);
                            scheduleGantt.setVisible(false);

                        } else {
                            this.getChildren().remove(scheduleWeek);
                        }
                    }

                    //Determine which type of table to create
                    if(newValue.equals("Department")) {
                        int selectedDept = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1;
                        createDepartmentTable(selectedDept, "Course",
                                menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString());
                    }
                    else if(newValue.equals("Professor")) {
                        int selectedDept = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1;
                        createProfessorTable(selectedDept, 0,
                                menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString());
                    }
                    else if(newValue.equals("Room")) {
                        int selectedDept = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1;
                        createRoomTable(selectedDept, 0,
                                menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString());
                    }
                    setScheduleSettings(newValue.toString(),
                            menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString(),
                            menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1,
                            times);
                    menuBar.requestFocus();
                    menuBar.getCb_scheduleGroup().requestFocus();

                }
        );

        //Combo Box For Filtering Departments
        deptNames.add("All");
        for (Department dept : departments) {
            deptNames.add(dept.getDepartmentName());
        }

        comboBoxDepartments = FXCollections.observableArrayList( deptNames );

        menuBar.setCb_scheduleDepartment(new ComboBox(comboBoxDepartments));

        //Crate a combobox for the selection of a department
        menuBar.getCb_scheduleDepartment().getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                    int deptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1;
                    String style = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedItem().toString();
                    createDepartmentTable(deptIndex, style,
                            menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString());
                }
        );


        individuals.add("Course");
        individuals.add("Professor");
        comboBoxIndividual = FXCollections.observableArrayList(individuals);
        menuBar.setCb_scheduleIndividual(new ComboBox(comboBoxIndividual));

        //Crate a combobox for selecting the individual whose data will be represented
        menuBar.getCb_scheduleIndividual().getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
            SelectionModel sm = menuBar.getCb_scheduleIndividual().getSelectionModel();
            if(sm.getSelectedItem() != oldValue) { //If there is a new selection determine whose data to represent
                if(sm.getSelectedItem() == "Professor") {
                    int selectedDept = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1;
                    createDepartmentTable(selectedDept,
                            sm.getSelectedItem().toString(),
                            menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString());
                }
                else if(sm.getSelectedItem() == "Course") {
                    int selectedDept = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex()-1;
                    createDepartmentTable(selectedDept,
                            sm.getSelectedItem().toString(),
                            menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString());
                }
            }
        });

        myStage = new Stage();
        // Build the default schedule based on a department
        if(departments.size() > 0) {
            scheduleGantt = new ScheduleGantt(times,
                    uni.getCourseList(),
                    uni.getBuildingList(),
                    menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedItem().toString());
            //Disable report button
            menuBar.disableReport();

            this.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
            //root.setPadding(new Insets(4, 4, 4, 4));
            tableScene = new Scene(this, 800, 500);
            tableScene.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());

            //Set table to fill the center of the stage
            scheduleGantt.prefWidthProperty().bind(myStage.widthProperty());
            scheduleGantt.prefHeightProperty().bind(myStage.heightProperty());

            this.setCenter(scheduleGantt);
            this.setTop(menuBar);
        }
        else{
            System.out.println("Error in ScheduleWindow; there are no departments.");
        }
    }


    //Update CourseScheduling.CourseScheduler.GUI to reflect select by dept, prof, or room
    private void setScheduleSettings(String entity, String currentScheduleStyle, int selectedDept, ArrayList<TimePeriod> times) {

        if(selectedDept > -1) { //If dept is -1 that means all departments are to be displayed
            Department dept = university.getDepartment(selectedDept);
            switch (entity) {
                case "Professor":
                    //Initialize schedule by professor combo box
                    ComboBox cb_newProf = new ComboBox();
                    ObservableList<String> scheduleByProfessorList = FXCollections.observableArrayList(new ArrayList<>());
                    for (int i = 0; i < dept.getSizeofInstructorsList(); i++) {
                        scheduleByProfessorList.add(dept.getInstructor(i).getFirstName() + " " + dept.getInstructor(i).getLastName());
                    }
                    cb_newProf.setItems(scheduleByProfessorList);
                    cb_newProf.getSelectionModel().selectFirst();
                    //Add selection listener to schedule by professor combo box
                    cb_newProf.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                this.getChildren().remove(scheduleWeek);
                                int instrIndex = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex();
                                createProfessorTable(selectedDept, instrIndex, currentScheduleStyle);

                            }
                    );
                    menuBar.setCb_scheduleIndividual(cb_newProf);
                    //Create the department selector
                    ComboBox cb_dept = new ComboBox();
                    ObservableList<String> obs_depts = FXCollections.observableArrayList(new ArrayList<>());
                    obs_depts.add("All");
                    for (Department department : departments) {
                        obs_depts.add(department.getDepartmentName());
                    }
                    cb_dept.setItems(obs_depts);
                    //Add selection listener to schedule by professor combo box
                    cb_dept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                //Get selected department
                                int deptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex() - 1;
                                //Get selected instructor
                                int instrIndex = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex();
                                createProfessorTable(deptIndex, instrIndex, currentScheduleStyle);
                            }
                    );

                    menuBar.setCb_scheduleDepartment(cb_dept);

                    if (currentScheduleStyle.equals("List")) {
                        //Set print report button
                        Button report = menuBar.getB_printReport();

                        //If the button was disabled, enable it.
                        if (report.isDisabled()) {
                            report.setDisable(false);
                        }

                        report.setOnAction((ActionEvent event) -> {
                            String dirPath;
                            dirPath = chooseDirectory();
                            scheduleList.printReport(dirPath);
                            reportDonePopup();
                        });
                        report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                    } else {
                        //Disable button
                        menuBar.getB_printReport().setDisable(true);
                        menuBar.getB_printReport().setTooltip(new Tooltip("Printed report not available for this chart."));

                    }
                    break;
                case "Room":
                    //Initialize schedule by professor combo box
                    ComboBox cb_newRoom = new ComboBox();
                    ObservableList<String> scheduleByRoomList = FXCollections.observableArrayList(new ArrayList<>());
                    for (int i = 0; i < dept.getSizeofBuildingList(); i++) {
                        for (int j = 0; j < dept.getBuilding(i).getRoomList().size(); j++) {
                            scheduleByRoomList.add(dept.getBuilding(i).getRoomList().get(j).getRoomInfo());
                        }
                    }
                    cb_newRoom.setItems(scheduleByRoomList);
                    cb_newRoom.getSelectionModel().selectFirst();
                    //Add selection listener to schedule by professor combo box
                    cb_newRoom.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                        int selectedRoom = -1;
                        int selDeptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex() - 1;

                        if (newValue != null) {

                            this.getChildren().remove(scheduleWeek);

                            if (selDeptIndex > -1) {
                                Department selDept = null;
                                selDept = university.getDepartment(selDeptIndex);

                                String roomInfo = newValue.toString();
                                //Search for room in room list
                                int roomIndex = 0;
                                while (selectedRoom == -1 && roomIndex < selDept.getRoomList().size()) {
                                    //System.out.println(selDept.getRoomList().get(roomIndex).getRoomInfo());
                                    if (selDept.getRoomList().get(roomIndex).getRoomInfo().equals(roomInfo)) {
                                        selectedRoom = roomIndex;
                                    }
                                    roomIndex++;
                                }
                            }
                            else {
                                String roomInfo = newValue.toString();
                                //Search for room in room list
                                int roomIndex = 0;
                                while (selectedRoom == -1 && roomIndex < university.getRoomList().size()) {
                                    //System.out.println(selDept.getRoomList().get(roomIndex).getRoomInfo());
                                    if (university.getRoomList().get(roomIndex).getRoomInfo().equals(roomInfo)) {
                                        selectedRoom = roomIndex;
                                    }
                                    roomIndex++;
                                }
                            }
                            createRoomTable(selDeptIndex, selectedRoom, currentScheduleStyle);
                        }
                    });

                    menuBar.setCb_scheduleIndividual(cb_newRoom);

                    //Create department filterer
                    cb_dept = new ComboBox();
                    obs_depts = FXCollections.observableArrayList(new ArrayList<>());
                    obs_depts.add("All");
                    for (Department department : departments) {
                        obs_depts.add(department.getDepartmentName());
                    }
                    cb_dept.setItems(obs_depts);
                    //Add selection listener to schedule  combo box
                    cb_dept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                Department selDept = null;
                                if(newValue.toString().equals("All")) {
                                    ObservableList<String> obs_rooms = FXCollections.observableArrayList(new ArrayList<>());
                                    for (int i = 0; i < university.getBuildingList().size(); i++) {
                                        for (int j = 0; j < university.getBuildingList().get(i).getRoomList().size(); j++) {
                                            obs_rooms.add(university.getBuildingList().get(i).getRoomList().get(j).getRoomInfo());
                                            menuBar.getCb_scheduleIndividual().setItems(obs_rooms);
                                            menuBar.getCb_scheduleIndividual().getSelectionModel().select(0);
                                        }
                                    }
                                }
                                else {
                                    for (Department aDept : departments) {
                                        if (aDept.getDepartmentName().equals(newValue.toString())) {
                                            selDept = aDept;
                                            ObservableList<String> obs_rooms = FXCollections.observableArrayList(new ArrayList<>());
                                            for (int i = 0; i < selDept.getSizeofBuildingList(); i++) {
                                                for (int j = 0; j < selDept.getBuilding(i).getRoomList().size(); j++) {
                                                    obs_rooms.add(selDept.getBuilding(i).getRoomList().get(j).getRoomInfo());
                                                    menuBar.getCb_scheduleIndividual().setItems(obs_rooms);
                                                    menuBar.getCb_scheduleIndividual().getSelectionModel().select(0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                    );

                    menuBar.setCb_scheduleDepartment(cb_dept);

                    if (currentScheduleStyle.equals("List")) {
                        //Set print report button
                        Button report = menuBar.getB_printReport();

                        //If the button was disabled, enable it.
                        if (report.isDisabled()) {
                            report.setDisable(false);
                        }

                        report.setOnAction((ActionEvent event) -> {
                            String dirPath;
                            dirPath = chooseDirectory();
                            scheduleList.printReport(dirPath);
                            reportDonePopup();
                        });
                        report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                    } else {
                        //Disable button
                        menuBar.disableReport();

                    }

                    break;
                case "Department":
                    ArrayList<String> individuals = new ArrayList<>();
                    individuals.add("Course");
                    individuals.add("Professor");
                    ObservableList<String> comboBoxIndividual = FXCollections.observableArrayList(individuals);
                    if (currentScheduleStyle.equals("Chart")) {

                        ComboBox cb_newDept = new ComboBox(comboBoxIndividual);

                        cb_newDept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                            SelectionModel sm = cb_newDept.getSelectionModel();
                            if (sm.getSelectedItem() != oldValue) {
                                createDepartmentTable(selectedDept, sm.getSelectedItem().toString(), currentScheduleStyle);
                            }
                        });
                        //Listener
                        menuBar.setCb_scheduleIndividual(cb_newDept);

                        cb_dept = new ComboBox();
                        obs_depts = FXCollections.observableArrayList(new ArrayList<>());
                        obs_depts.add("All");
                        for (Department department : departments) {
                            obs_depts.add(department.getDepartmentName());
                        }
                        cb_dept.setItems(obs_depts);
                        //Add selection listener to schedule by professor combo box
                        cb_dept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                    int deptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex() - 1;
                                    String style = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedItem().toString();
                                    createDepartmentTable(deptIndex, style, currentScheduleStyle);
                                }
                        );

                        menuBar.setCb_scheduleDepartment(cb_dept);

                        //Disable button
                        menuBar.disableReport();

                    } else {
                        ArrayList<String> na = new ArrayList<>();
                        na.add("N/A");
                        menuBar.setCb_scheduleIndividual(new ComboBox(FXCollections.observableList(na)));
                        menuBar.getCb_scheduleIndividual().setDisable(true);

                        //Set print report button
                        Button report = menuBar.getB_printReport();

                        //If the button was disabled, enable it.
                        if (report.isDisabled()) {
                            report.setDisable(false);
                        }

                        report.setOnAction((ActionEvent event) -> {
                            String dirPath;
                            dirPath = chooseDirectory();
                            scheduleList.printReport(dirPath);
                            reportDonePopup();
                        });
                        report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                    }
                    break;
            }
        }
        else {
            switch (entity) {
                case "Professor":
                    //Initialize schedule by professor combo box
                    ComboBox cb_newProf = new ComboBox();
                    ObservableList<String> scheduleByProfessorList = FXCollections.observableArrayList(new ArrayList<>());
                    for (int i = 0; i < university.getInstructorList().size(); i++) {
                        scheduleByProfessorList.add(university.getInstructorList().get(i).getFirstName() + " " + university.getInstructorList().get(i).getLastName());
                    }
                    cb_newProf.setItems(scheduleByProfessorList);
                    cb_newProf.getSelectionModel().selectFirst();
                    //Add selection listener to schedule by professor combo box
                    cb_newProf.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                this.getChildren().remove(scheduleWeek);
                                int instrIndex = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex();
                                createProfessorTable(selectedDept, instrIndex, currentScheduleStyle);

                            }
                    );
                    menuBar.setCb_scheduleIndividual(cb_newProf);
                    //Create the department selector
                    ComboBox cb_dept = new ComboBox();
                    ObservableList<String> obs_depts = FXCollections.observableArrayList(new ArrayList<>());
                    obs_depts.add("All");
                    for (Department department : departments) {
                        obs_depts.add(department.getDepartmentName());
                    }
                    cb_dept.setItems(obs_depts);
                    //Add selection listener to schedule by professor combo box
                    cb_dept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                //Get selected department
                                int deptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex() - 1;
                                //Get selected instructor
                                int instrIndex = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex();
                                createProfessorTable(deptIndex, instrIndex, currentScheduleStyle);
                            }
                    );

                    menuBar.setCb_scheduleDepartment(cb_dept);

                    if (currentScheduleStyle.equals("List")) {
                        //Set print report button
                        Button report = menuBar.getB_printReport();

                        //If the button was disabled, enable it.
                        if (report.isDisabled()) {
                            report.setDisable(false);
                        }

                        report.setOnAction((ActionEvent event) -> {
                            String dirPath;
                            dirPath = chooseDirectory();
                            scheduleList.printReport(dirPath);
                            reportDonePopup();
                        });
                        report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                    } else {
                        //Disable button
                        menuBar.getB_printReport().setDisable(true);
                        menuBar.getB_printReport().setTooltip(new Tooltip("Printed report not available for this chart."));

                    }
                    break;
                case "Room":
                    //Initialize schedule by professor combo box
                    ComboBox cb_newRoom = new ComboBox();
                    ObservableList<String> scheduleByRoomList = FXCollections.observableArrayList(new ArrayList<>());
                    for (int i = 0; i < university.getBuildingList().size(); i++) {
                        for (int j = 0; j < university.getBuildingList().get(i).getRoomList().size(); j++) {
                            scheduleByRoomList.add(university.getBuildingList().get(i).getRoomList().get(j).getRoomInfo());
                        }
                    }
                    cb_newRoom.setItems(scheduleByRoomList);
                    cb_newRoom.getSelectionModel().selectFirst();
                    //Add selection listener to schedule by professor combo box
                    cb_newRoom.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                        int selectedRoom = -1;
                        int selDeptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex() - 1;

                        if (newValue != null) {

                            this.getChildren().remove(scheduleWeek);

                            if (selDeptIndex > -1) {
                                Department selDept = null;
                                selDept = university.getDepartment(selDeptIndex);

                                String roomInfo = newValue.toString();
                                //Search for room in room list
                                int roomIndex = 0;
                                while (selectedRoom == -1 && roomIndex < selDept.getRoomList().size()) {
                                    //System.out.println(selDept.getRoomList().get(roomIndex).getRoomInfo());
                                    if (selDept.getRoomList().get(roomIndex).getRoomInfo().equals(roomInfo)) {
                                        selectedRoom = roomIndex;
                                    }
                                    roomIndex++;
                                }
                            }
                            else {
                                String roomInfo = newValue.toString();
                                //Search for room in room list
                                int roomIndex = 0;
                                while (selectedRoom == -1 && roomIndex < university.getRoomList().size()) {
                                    //System.out.println(selDept.getRoomList().get(roomIndex).getRoomInfo());
                                    if (university.getRoomList().get(roomIndex).getRoomInfo().equals(roomInfo)) {
                                        selectedRoom = roomIndex;
                                    }
                                    roomIndex++;
                                }
                            }
                            createRoomTable(selDeptIndex, selectedRoom, currentScheduleStyle);
                        }
                    });

                    menuBar.setCb_scheduleIndividual(cb_newRoom);

                    //Create department filterer
                    cb_dept = new ComboBox();
                    obs_depts = FXCollections.observableArrayList(new ArrayList<>());
                    obs_depts.add("All");
                    for (Department department : departments) {
                        obs_depts.add(department.getDepartmentName());
                    }
                    cb_dept.setItems(obs_depts);
                    //Add selection listener to schedule  combo box
                    cb_dept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                Department selDept = null;
                                if(newValue.toString().equals("All")) {
                                    ObservableList<String> obs_rooms = FXCollections.observableArrayList(new ArrayList<>());
                                    for (int i = 0; i < university.getBuildingList().size(); i++) {
                                        for (int j = 0; j < university.getBuildingList().get(i).getRoomList().size(); j++) {
                                            obs_rooms.add(university.getBuildingList().get(i).getRoomList().get(j).getRoomInfo());
                                            menuBar.getCb_scheduleIndividual().setItems(obs_rooms);
                                            menuBar.getCb_scheduleIndividual().getSelectionModel().select(0);
                                        }
                                    }
                                }
                                else {
                                    for (Department aDept : departments) {
                                        if (aDept.getDepartmentName().equals(newValue.toString())) {
                                            selDept = aDept;
                                            ObservableList<String> obs_rooms = FXCollections.observableArrayList(new ArrayList<>());
                                            for (int i = 0; i < selDept.getSizeofBuildingList(); i++) {
                                                for (int j = 0; j < selDept.getBuilding(i).getRoomList().size(); j++) {
                                                    obs_rooms.add(selDept.getBuilding(i).getRoomList().get(j).getRoomInfo());
                                                    menuBar.getCb_scheduleIndividual().setItems(obs_rooms);
                                                    menuBar.getCb_scheduleIndividual().getSelectionModel().select(0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                    );

                    menuBar.setCb_scheduleDepartment(cb_dept);

                    if (currentScheduleStyle.equals("List")) {
                        //Set print report button
                        Button report = menuBar.getB_printReport();

                        //If the button was disabled, enable it.
                        if (report.isDisabled()) {
                            report.setDisable(false);
                        }

                        report.setOnAction((ActionEvent event) -> {
                            String dirPath;
                            dirPath = chooseDirectory();
                            scheduleList.printReport(dirPath);
                            reportDonePopup();
                        });
                        report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                    } else {
                        //Disable button
                        menuBar.disableReport();

                    }

                    break;
                case "Department":
                    ArrayList<String> individuals = new ArrayList<>();
                    individuals.add("Course");
                    individuals.add("Professor");
                    ObservableList<String> comboBoxIndividual = FXCollections.observableArrayList(individuals);
                    if (currentScheduleStyle.equals("Chart")) {

                        ComboBox cb_newDept = new ComboBox(comboBoxIndividual);

                        cb_newDept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                            SelectionModel sm = cb_newDept.getSelectionModel();
                            if (sm.getSelectedItem() != oldValue) {
                                createDepartmentTable(selectedDept, sm.getSelectedItem().toString(), currentScheduleStyle);
                            }
                        });
                        //Listener
                        menuBar.setCb_scheduleIndividual(cb_newDept);

                        cb_dept = new ComboBox();
                        obs_depts = FXCollections.observableArrayList(new ArrayList<>());
                        obs_depts.add("All");
                        for (Department department : departments) {
                            obs_depts.add(department.getDepartmentName());
                        }
                        cb_dept.setItems(obs_depts);
                        //Add selection listener to schedule by professor combo box
                        cb_dept.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                                    int deptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex() - 1;
                                    String style = menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedItem().toString();
                                    createDepartmentTable(deptIndex, style, currentScheduleStyle);
                                }
                        );

                        menuBar.setCb_scheduleDepartment(cb_dept);

                        //Disable button
                        menuBar.disableReport();

                    } else {
                        ArrayList<String> na = new ArrayList<>();
                        na.add("N/A");
                        menuBar.setCb_scheduleIndividual(new ComboBox(FXCollections.observableList(na)));
                        menuBar.getCb_scheduleIndividual().setDisable(true);

                        //Set print report button
                        Button report = menuBar.getB_printReport();

                        //If the button was disabled, enable it.
                        if (report.isDisabled()) {
                            report.setDisable(false);
                        }

                        report.setOnAction((ActionEvent event) -> {
                            String dirPath;
                            dirPath = chooseDirectory();
                            scheduleList.printReport(dirPath);
                            reportDonePopup();
                        });
                        report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                    }
                    break;
            }
        }
    }

    //Create week table for professor
    private void createProfessorTable(int selectedDept, int selectedInstructor, String currentScheduleStyle) {

        Instructor instr;
        if(selectedDept > -1) {
            instr = university.getDepartment(selectedDept).getInstructor(selectedInstructor);

            if (currentScheduleStyle.equals("Chart")) {
                scheduleWeek = new ScheduleWeek(instr, times);
                //Set table to fill center stage
                scheduleWeek.prefWidthProperty().bind(myStage.widthProperty());
                scheduleWeek.prefHeightProperty().bind(myStage.heightProperty());
                //Disable button
                menuBar.disableReport();

                //Add to stage
                this.setCenter(scheduleWeek);
                myStage.setScene(tableScene);
            } else if (currentScheduleStyle.equals("List")) {

                scheduleList = new ScheduleList(instr.getCourseList());
                //Set table to fill the center of the stage
                scheduleList.prefWidthProperty().bind(myStage.widthProperty());
                scheduleList.prefHeightProperty().bind(myStage.heightProperty());
                //Set print report button
                Button report = menuBar.getB_printReport();

                //If the button was disabled, enable it.
                if (report.isDisabled()) {
                    report.setDisable(false);
                }

                report.setOnAction((ActionEvent event) -> {
                    String dirPath;
                    dirPath = chooseDirectory();
                    scheduleList.printReport(dirPath);
                    reportDonePopup();
                });
                report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                //Set stage
                this.setCenter(scheduleList);
                myStage.setScene(tableScene);
            }
        }
        else {
            instr = university.getInstructorList().get(selectedInstructor);
            if (currentScheduleStyle.equals("Chart")) {
                scheduleWeek = new ScheduleWeek(instr, times);
                //Set table to fill center stage
                scheduleWeek.prefWidthProperty().bind(myStage.widthProperty());
                scheduleWeek.prefHeightProperty().bind(myStage.heightProperty());
                //Disable button
                menuBar.disableReport();

                //Add to stage
                this.setCenter(scheduleWeek);
                myStage.setScene(tableScene);
            } else if (currentScheduleStyle.equals("List")) {

                scheduleList = new ScheduleList(instr.getCourseList());
                //Set table to fill the center of the stage
                scheduleList.prefWidthProperty().bind(myStage.widthProperty());
                scheduleList.prefHeightProperty().bind(myStage.heightProperty());
                //Set print report button
                Button report = menuBar.getB_printReport();

                //If the button was disabled, enable it.
                if (report.isDisabled()) {
                    report.setDisable(false);
                }

                report.setOnAction((ActionEvent event) -> {
                    String dirPath;
                    dirPath = chooseDirectory();
                    scheduleList.printReport(dirPath);
                    reportDonePopup();
                });
                report.setTooltip(new Tooltip("Print a report of this schedule to a file."));
                //Set stage
                this.setCenter(scheduleList);
                myStage.setScene(tableScene);
            }
        }
    }

    //Create week table for room
    private void createRoomTable(int selectedDepartment, int selectedRoom, String currentScheduleStyle) {

        if(selectedDepartment > -1) {
            Room rm = university.getDepartment(selectedDepartment).getRoomList().get(selectedRoom);
            if (currentScheduleStyle.equals("Chart")) {
                scheduleWeek = new ScheduleWeek(rm, times);
                //Set table to fill center stage
                scheduleWeek.prefWidthProperty().bind(myStage.widthProperty());
                scheduleWeek.prefHeightProperty().bind(myStage.heightProperty());
                //Disable button
                menuBar.disableReport();

                //Set stage
                this.setCenter(scheduleWeek);
                myStage.setScene(tableScene);
            } else if (currentScheduleStyle.equals("List")) {
                scheduleList = new ScheduleList(rm.getCourseList());
                //Set table to fill the center of the stage
                scheduleList.prefWidthProperty().bind(myStage.widthProperty());
                scheduleList.prefHeightProperty().bind(myStage.heightProperty());
                //Set print report button
                Button report = menuBar.getB_printReport();

                //If the button was disabled, enable it.
                if (report.isDisabled()) {
                    report.setDisable(false);
                }

                report.setOnAction((ActionEvent event) -> {
                    String dirPath;
                    dirPath = chooseDirectory();
                    scheduleList.printReport(dirPath);
                    reportDonePopup();
                });
                report.setTooltip(new Tooltip("Print a report of this schedule to a file."));

                //Set stage
                this.setCenter(scheduleList);
                myStage.setScene(tableScene);
            }
        }
        else {
            Room rm = university.getRoomList().get(selectedRoom);
            if (currentScheduleStyle.equals("Chart")) {
                scheduleWeek = new ScheduleWeek(rm, times);
                //Set table to fill center stage
                scheduleWeek.prefWidthProperty().bind(myStage.widthProperty());
                scheduleWeek.prefHeightProperty().bind(myStage.heightProperty());
                //Disable button
                menuBar.disableReport();

                //Set stage
                this.setCenter(scheduleWeek);
                myStage.setScene(tableScene);
            } else if (currentScheduleStyle.equals("List")) {
                scheduleList = new ScheduleList(rm.getCourseList());
                //Set table to fill the center of the stage
                scheduleList.prefWidthProperty().bind(myStage.widthProperty());
                scheduleList.prefHeightProperty().bind(myStage.heightProperty());
                //Set print report button
                Button report = menuBar.getB_printReport();

                //If the button was disabled, enable it.
                if (report.isDisabled()) {
                    report.setDisable(false);
                }

                report.setOnAction((ActionEvent event) -> {
                    String dirPath;
                    dirPath = chooseDirectory();
                    scheduleList.printReport(dirPath);
                    reportDonePopup();
                });
                report.setTooltip(new Tooltip("Print a report of this schedule to a file."));

                //Set stage
                this.setCenter(scheduleList);
                myStage.setScene(tableScene);
            }
        }
    }

    // Creates department table
    public void createDepartmentTable(int selectedDept, String displayOption, String currentScheduleStyle){

        if(selectedDept > -1) {
            Department dept = departments.get(selectedDept);
            if (currentScheduleStyle.equals("Chart")) {
                scheduleGantt = new ScheduleGantt(times,
                        dept.getCourseList(), dept.getBuildingList(), displayOption);


                //Set table to fill the center of the stage
                scheduleGantt.prefWidthProperty().bind(myStage.widthProperty());
                scheduleGantt.prefHeightProperty().bind(myStage.heightProperty());
                //Disable button
                menuBar.disableReport();

                //Set stage
                this.setCenter(scheduleGantt);
                myStage.setScene(tableScene);
            } else if (currentScheduleStyle.equals("List")) {
                scheduleList = new ScheduleList(dept.getCourseList());
                //Set table to fill the center of the stage
                scheduleList.prefWidthProperty().bind(myStage.widthProperty());
                scheduleList.prefHeightProperty().bind(myStage.heightProperty());
                //Remove unavailable option
                ArrayList<String> na = new ArrayList<>();
                na.add("N/A");
                menuBar.setCb_scheduleIndividual(new ComboBox(FXCollections.observableList(na)));
                menuBar.getCb_scheduleIndividual().setDisable(true);
                //Set print report button
                Button report = menuBar.getB_printReport();

                //If the button was disabled, enable it.
                if (report.isDisabled()) {
                    report.setDisable(false);
                }

                report.setOnAction((ActionEvent event) -> {
                    String dirPath;
                    dirPath = chooseDirectory();
                    scheduleList.printReport(dirPath);
                    reportDonePopup();
                });
                report.setTooltip(new Tooltip("Print a report of this schedule to a file."));

                //Set stage
                this.setCenter(scheduleList);
                myStage.setScene(tableScene);
            }
        }
        else {
            if (currentScheduleStyle.equals("Chart")) {
                scheduleGantt = new ScheduleGantt(times,
                        university.getCourseList(), university.getBuildingList(), displayOption);


                //Set table to fill the center of the stage
                scheduleGantt.prefWidthProperty().bind(myStage.widthProperty());
                scheduleGantt.prefHeightProperty().bind(myStage.heightProperty());
                //Disable button
                menuBar.disableReport();

                //Set stage
                this.setCenter(scheduleGantt);
                myStage.setScene(tableScene);
            } else if (currentScheduleStyle.equals("List")) {
                scheduleList = new ScheduleList(university.getCourseList());
                //Set table to fill the center of the stage
                scheduleList.prefWidthProperty().bind(myStage.widthProperty());
                scheduleList.prefHeightProperty().bind(myStage.heightProperty());
                //Remove unavailable option
                ArrayList<String> na = new ArrayList<>();
                na.add("N/A");
                menuBar.setCb_scheduleIndividual(new ComboBox(FXCollections.observableList(na)));
                menuBar.getCb_scheduleIndividual().setDisable(true);
                //Set print report button
                Button report = menuBar.getB_printReport();

                //If the button was disabled, enable it.
                if (report.isDisabled()) {
                    report.setDisable(false);
                }

                report.setOnAction((ActionEvent event) -> {
                    String dirPath;
                    dirPath = chooseDirectory();
                    scheduleList.printReport(dirPath);
                });
                report.setTooltip(new Tooltip("Print a report of this schedule to a file."));

                //Set stage
                this.setCenter(scheduleList);
                myStage.setScene(tableScene);
            }
        }
    }

    public String chooseDirectory() {
        
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(myStage);
        
        return selectedDirectory.getAbsolutePath();
    }

    public void reportDonePopup() {
        Stage popupWindow = new Stage();
        Label label = new Label("Printed Reports Complete");
        Button reportButton1 = new Button("OK");
        reportButton1.setOnAction(e -> popupWindow.close());
        
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, reportButton1);
        layout.setAlignment(Pos.CENTER);
        Scene scene1 = new Scene(layout, 200, 100);
        layout.getStylesheets().add(getClass().getResource("ReportPopupCSS.css").toExternalForm());
        popupWindow.setScene(scene1);
        popupWindow.showAndWait();
    }


    //If manual data entry is given it is necessary to update the table.
    public void refreshTable() {
        departments = university.getDepts();
        int deptIndex = -1;
        deptIndex = -1;
        if(menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedItem().toString().equals("All")){
            deptIndex = -1;
        }
        else {
            deptIndex = menuBar.getCb_scheduleDepartment().getSelectionModel().getSelectedIndex();
        }
        switch(menuBar.getCb_scheduleGroup().getSelectionModel().getSelectedItem().toString()) {
            case "Department":
                createDepartmentTable(deptIndex, //department
                        menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedItem().toString(), //individual
                        menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString()); //schedule style

                break;
            case "Room":
                createRoomTable(deptIndex,//department
                        menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex(),//individual
                        menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString()); //schedule style

                break;
            case "Professor":
                createProfessorTable(deptIndex,//department
                        menuBar.getCb_scheduleIndividual().getSelectionModel().getSelectedIndex(),//individual
                        menuBar.getCb_scheduleStyle().getSelectionModel().getSelectedItem().toString()); //schedule style
                break;
        }

    }
}

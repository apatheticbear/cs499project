package CourseScheduler.GUI;


import CourseScheduler.UniversityParts.Course;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;

public class CourseInfoPopup extends GridPane {
    private PseudoClass headerStyle = PseudoClass.getPseudoClass("header");

    CourseInfoPopup(Course c) {
        //this.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());

        //Create the header for the data
        ArrayList<String> properties = new ArrayList<>();
        properties.add("Course");
        properties.add("Section");
        properties.add("Professor");
        properties.add("Time");
        properties.add("Day");
        properties.add("Room");
        properties.add("Building");
        properties.add("Department");
        properties.add("Expected Enrollment");



        ObservableList obs_properties = FXCollections.observableArrayList(properties);

        ListView header = new ListView();
        header.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        header.setItems(obs_properties);
        header.pseudoClassStateChanged(headerStyle, true);

        //Create the data list
        ArrayList dataList = new ArrayList<>();
        dataList.add(c.getClassName());
        dataList.add(c.getSectionNumber());
        dataList.add(c.getProfessorFullName());
        dataList.add(c.getStringClassTime());
        dataList.add(c.getDays());
        dataList.add(c.getRoomNum());
        dataList.add(c.getBuildingName());
        dataList.add(c.getDepartmentName());
        dataList.add(c.getExpectedEnrollment());


        ObservableList obs_dataList = FXCollections.observableArrayList(dataList);

        ListView data = new ListView();
        data.setItems(obs_dataList);
        data.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());

        data.pseudoClassStateChanged(headerStyle, false);

        header.setPrefSize(175,225);
        data.setPrefSize  (125,225);


        this.add(header, 0,0);
        this.add(data, 1,0);

    }

}

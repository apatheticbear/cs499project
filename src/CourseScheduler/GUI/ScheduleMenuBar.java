package CourseScheduler.GUI;
/*
    Written by Tristan Davis
        This class is written to help manage the many states of the menu bar in the ScheduleWindow class. This
        class helps manage the states by giving a centralized area to manipulate the style and layout of the
        menu bar without having to redefine the layout for each change made to the menu bar.
 */
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

import java.util.ArrayList;

public class ScheduleMenuBar extends HBox {

    private ComboBox cb_scheduleStyle;
    private ComboBox cb_scheduleGroup;
    private ComboBox cb_scheduleDepartment;
    private ComboBox cb_scheduleIndividual;

    private Button b_printReport;

    private Label l_scheduleStyle;
    private Label l_scheduleGroup;
    private Label l_scheduleDepartment;
    private Label l_scheduleIndividual;


    ScheduleMenuBar() {
        this.setSpacing(2.0);
        this.setPadding(new Insets(4, 4, 4, 4));
        this.setStyle("-fx-background-color: #55759e;");

        l_scheduleStyle      = new Label("Style");
        l_scheduleGroup      = new Label("Group");
        l_scheduleDepartment = new Label("Department");
        l_scheduleIndividual = new Label("Individual");

        cb_scheduleStyle      = new ComboBox();
        cb_scheduleGroup      = new ComboBox();
        cb_scheduleDepartment = new ComboBox();
        cb_scheduleIndividual = new ComboBox();

        b_printReport = new Button("Print Report");


        //Declare list of schedule styles
        ArrayList<String> scheduleStyles = new ArrayList();
        ObservableList<String> observableScheduleStyles;

        scheduleStyles.add("Chart");
        scheduleStyles.add("List");

        observableScheduleStyles = FXCollections.observableArrayList(scheduleStyles);
        cb_scheduleStyle.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        cb_scheduleStyle.getSelectionModel().selectFirst();


    }

    //Combobox settings
    private void comboBoxSettings(ComboBox cb) {
        cb.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        cb.getSelectionModel().selectFirst();
        cb_scheduleGroup.setPrefWidth(115);
        cb_scheduleIndividual.setPrefWidth(150);
    }

    public ComboBox getCb_scheduleStyle() {
        return cb_scheduleStyle;
    }

    public void setCb_scheduleStyle(ComboBox cb_scheduleStyle) {
        unload();
        this.cb_scheduleStyle = cb_scheduleStyle;
        comboBoxSettings(this.cb_scheduleStyle);
        load();
    }

    public ComboBox getCb_scheduleGroup() {
        return cb_scheduleGroup;
    }

    public void setCb_scheduleGroup(ComboBox cb_scheduleGroup) {
        unload();
        this.cb_scheduleGroup = cb_scheduleGroup;
        comboBoxSettings(this.cb_scheduleGroup);

        load();
    }

    public ComboBox getCb_scheduleDepartment() {
        return cb_scheduleDepartment;

    }

    public void setCb_scheduleDepartment(ComboBox cb_scheduleDepartment) {
        unload();
        this.cb_scheduleDepartment = cb_scheduleDepartment;
        comboBoxSettings(this.cb_scheduleDepartment);
        load();
    }

    public ComboBox getCb_scheduleIndividual() {
        return cb_scheduleIndividual;
    }

    public void setCb_scheduleIndividual(ComboBox cb_scheduleIndividual) {
        unload();
        this.cb_scheduleIndividual = cb_scheduleIndividual;
        comboBoxSettings(this.cb_scheduleIndividual);
        load();
    }

    public Button getB_printReport() {
        return b_printReport;
    }

    public void disableReport() {
        //Disable button
        this.b_printReport.setDisable(true);
        this.b_printReport.setTooltip(new Tooltip("Printed report not available for this chart."));
    }

    public void setB_printReport(Button b_printReport) {
        unload();
        this.b_printReport = b_printReport;
        this.b_printReport.getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
        load();
    }

    private void unload() {
        //Remove stuff
        this.getChildren().removeAll(l_scheduleStyle, cb_scheduleStyle,
                l_scheduleGroup, cb_scheduleGroup,
                l_scheduleDepartment, cb_scheduleDepartment,
                l_scheduleIndividual, cb_scheduleIndividual,
                b_printReport);
    }

    private void load() {
        //add Stuff

        this.getChildren().addAll(l_scheduleStyle, cb_scheduleStyle,
                l_scheduleGroup, cb_scheduleGroup,
                l_scheduleDepartment, cb_scheduleDepartment,
                l_scheduleIndividual, cb_scheduleIndividual,
                b_printReport);
    }

}
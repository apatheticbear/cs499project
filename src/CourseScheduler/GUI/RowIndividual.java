package CourseScheduler.GUI;

import CourseScheduler.UniversityParts.Course;
import CourseScheduler.UniversityParts.Day;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import java.util.ArrayList;
import java.util.List;

/*
    This class represent a row in the ScheduleWeek table.
    It maintains an array of scroll panes representing a class across the days.
    Those scroll panes hold buttons representing courses.
 */

public class RowIndividual {
    private String period;
    private ScrollPane[] classes;
    private boolean isHighlighted;
    private PseudoClass highlightScrollpane = PseudoClass.getPseudoClass("highlight");
    private ScheduleWeek sw;


    RowIndividual(String classPeriod, ScheduleWeek newSW) {
        sw = newSW;
        period = classPeriod;
        classes = new ScrollPane[5];
        for(int i = 0; i < 5; i++) {
            VBox box = new VBox();
            box.setSpacing(4);
            classes[i] = new ScrollPane();
            classes[i].getStylesheets().add(getClass().getResource("CourseSchedulingSystemCSS.css").toExternalForm());
            classes[i].pseudoClassStateChanged(highlightScrollpane, false);
            classes[i].setContent(box);
        }


    }


    //Add a class to the row
    void addClass(Course newCourse) {
        ArrayList<Day.day> theDays = newCourse.getDaysArray();
        if(theDays != null) {
            for(Day.day aDay : theDays) {
                Button button = new Button(newCourse.getClassAndSection());
                button.setOnAction((ActionEvent event) -> {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);

                    alert.setTitle("Course Information Dialog");
                    alert.setHeaderText("Course Detail");
                    alert.getDialogPane().setContent(new CourseInfoPopup(newCourse));

                    alert.showAndWait();
                });
                VBox box = (VBox) classes[aDay.ordinal()].getContent();
                box.getChildren().add(button);
            }
        }
    }

    //Get class at day and period
    ScrollPane getClass(String day) {
        ScrollPane className;
        switch(day) {
            case "Monday":
                className = classes[0];

                break;
            case "Tuesday":
                className = classes[1];

                break;
            case "Wednesday":
                className = classes[2];

                break;
            case "Thursday":
                className = classes[3];

                break;
            case "Friday":
                className = classes[4];

                break;
            default:
                return null;
        }
        return className;
    }

    public String getPeriod() { return period; }

    //function to set the highlight style if there is a conflict
    public void highlightRow(boolean highlight) {

        if(!this.isHighlighted) { //If this row isn't highlighted yet we need to go back and highlight other scrollpanes
            for (int i = 0; i < 5; i++) {
                classes[i].setPrefSize(85,75); //Make it taller
                classes[i].pseudoClassStateChanged(highlightScrollpane, true);
            }
        }
        if(highlight) {
            this.isHighlighted = true;
        }
        else {
            for (int i = 0; i < 5; i++) {
                classes[i].setPrefSize(85,50); //Make it shorter
                classes[i].pseudoClassStateChanged(highlightScrollpane, true);
            }

            this.isHighlighted = false;
        }
    }

    //Look for conflicts
    public void findConflicts() {
        for(int i = 0; i < 5; i++) //5 days in a week
        {
          ScrollPane tempPane =  classes[i];
          VBox aBox = (VBox) tempPane.getContent();
          List<Node> boxChildrenList = aBox.getChildren();
          if(boxChildrenList.size() > 1) {
              highlightRow(true);
              for(int k = 0; k < boxChildrenList.size(); k++) {
                 // System.out.println(boxChildrenList.get(k).getClass().toString());
                  if(boxChildrenList.get(k).getClass().toGenericString().equals("public class javafx.scene.control.Button")) {
                      Button b = (Button) boxChildrenList.get(k);
                      sw.setConflicts(b.getText());
                  }
              }
          }
          else {
              if(boxChildrenList.size() > 0 && this.isHighlighted()) {
                  Button b = (Button) boxChildrenList.get(0);
                  highlightRow(false);
                  sw.clearConflicts(b.getText());
              }
          }
        }
    }

    //get the variable used to manage the highlighted state
    public boolean isHighlighted() {
        return isHighlighted;
    }

}

package CourseScheduler.UniversityParts;

import java.util.ArrayList;

public class University {
    private ArrayList<Department> departments;
    private ArrayList<Building> buildings;
    private ArrayList<Room> rooms;
    private ArrayList<Instructor> instructors;
    private ArrayList<Course> courses;
    private ArrayList<TimePeriod> timePeriods;
    private int timeBetweenPeriods;
    private ArrayList<Day.day> days;


    public University() {
        departments = new ArrayList<>();
        buildings   = new ArrayList<>();
        rooms       = new ArrayList<>();
        instructors = new ArrayList<>();
        courses     = new ArrayList<>();
        timePeriods = new ArrayList(10);
        days = new ArrayList<Day.day>();
        days.add(Day.day.M); //MW
        days.add(Day.day.T); //T/TH

        timeBetweenPeriods = 0;

   }
    public ArrayList<Day.day> getDays() {
        return this.days;
   }

   //This needs to be removed.
   public ArrayList<Department> getDepts() {
        return departments;
    }

    public void addDepartment(Department dept) {
        //System.out.println(dept.getDepartmentName());
        departments.add(dept);
        dept.setId(departments.size()-1);
    }


    public Department getDepartment(String departmentName) {
        Department dept = null;
        int i = 0;
        while(i < departments.size() && dept == null) {
            if(departments.get(i).getDepartmentName().equals(departmentName)) {
                dept = departments.get(i);
            }
            i++;
        }
        if(dept == null) {
            System.out.println("Error in University: unable to find department: " + departmentName);
        }
        return dept;
    }

    public Department getDepartment(int departmentIndex) {
        return departments.get(departmentIndex);
    }

    public void addBuilding(String departmentName, Building bldg) {
            //Make sure duplicate buildings are not being added.
            boolean buildingFound = false;
            int foundIndex = 0;
            int buildingIndex = 0;
            //Search for buildings in building list
            while(buildingFound == false && buildingIndex < this.buildings.size()) {
                //If a building is found check to see if a new room needs to be added to that building
                //System.out.println(this.buildings.get(buildingIndex).getBuildingShortName()+"=="+bldg.getBuildingShortName());
                if(this.buildings.get(buildingIndex).getBuildingShortName().equals(bldg.getBuildingShortName())) {
                    buildingFound = true;
                    foundIndex = buildingIndex;
                    for(Room room : bldg.getRoomList()) {
                        boolean roomFound = false;
                        int roomIndex = 0;
                        //Search for the room: If a room is NOT found, add it to the list of rooms.
                        while(roomFound == false && roomIndex < this.buildings.get(buildingIndex).getRoomList().size()) {
                            //System.out.println(room.getRoomInfo()+"=="+this.buildings.get(buildingIndex).getRoomList().get(roomIndex).getRoomInfo());
                            if(room.getRoomInfo().equals(this.buildings.get(buildingIndex).getRoomList().get(roomIndex).getRoomInfo())) {
                                roomFound = true;
                            }
                            roomIndex++;
                        }
                        //If the room is not found add it to the buildings list of rooms.
                        if(roomFound == false) {
                            this.buildings.get(buildingIndex).getRoomList().add(room);
                        }
                    }
                }
                buildingIndex++;
            }
            //If the building does not already exist add it to the list of buildings.
            if(buildingFound == false) {
                buildings.add(bldg);
                Department dept = findDepartment(departmentName);
                if(dept != null) {
                    dept.addBuilding(bldg);
                }
             }
            else {
                Department dept = findDepartment(departmentName);
                if(dept != null) {
                     dept.addBuilding(buildings.get(foundIndex));
                }
            }
            bldg.setId(buildings.size()-1);
    }

    public int findBuilding(String buildingName) {
        int foundIndex = -1;
        int i = 0;
        while(foundIndex == -1 && i < buildings.size())
        {
            //System.out.println(buildings.get(i).getBuildingShortName() + "==" + buildingName);
            if(buildings.get(i).getBuildingShortName().equals(buildingName)) {
                foundIndex = i;
            }
            i++;
        }
        if(foundIndex > -1) {
            return foundIndex;
        }
        else{
            return -1;
        }

    }
    
    public ArrayList<Department> getDepartmentList(){
        return this.departments;
    }

    public Department findDepartment(String departmentName) {
        Department dept = null;
        boolean departmentFound = false;
        int departmentIndex = 0;
        while(departmentFound == false && departmentIndex < departments.size()) {
            if(departments.get(departmentIndex).getDepartmentName().equalsIgnoreCase(departmentName)) {
               dept = departments.get(departmentIndex);
            }
            departmentIndex++;
        }

        return dept;
    }

    public void addRoom(Room room){
        this.rooms.add(room);
        room.setId(rooms.size()-1);

    }




    /*
    Get and set instructors
     */

    public void addInstructor(String department, Instructor instr) {
        //Department chosenDept = getDepartment(department);
        instructors.add(instr);
        Department dept = findDepartment(department);
        if(dept != null) {
            dept.addInstructor(instr);
        }
        instr.setId(instructors.size()-1);
    }
    
    public Instructor getInstructor(String department, String instructor) {
        return null;
    }


    public Course getCourse(String department, String course) {
        return null;
    }

    public void setTimePeriods(ArrayList<TimePeriod> tps) {
        timePeriods = tps;
    }

    public ArrayList<TimePeriod> getTimePeriods() {
        return timePeriods;
    }

    public void setTimeBetweenPeriods(int tbps) {
        timeBetweenPeriods = tbps;
    }

    public int getTimeBetweenPeriods() {
        return timeBetweenPeriods;
    }


    public void addCourse(String department, Course c) {
        for(Course aCourse : courses) {
            if(aCourse.getClassName().equals(c.getClassName())) {
                c.setSectionNumber(aCourse.getSectionNumber() + 1);
            }
        }
        courses.add(c);
        Department dept = findDepartment(department);
        if(dept != null) {
            dept.addCourse(c);
        }
        c.setId(courses.size()-1);

    }

    public void insertRoom(String departmentName, String buildingName, Room room) {
        int buildingIndex = findBuilding(buildingName);
        Department dept = getDepartment(departmentName);

        if(buildingIndex > -1) {
            int roomIndex = -1;
            int i = 0;
            int departmentBuildingIndex = dept.findBuilding(buildingName);

            while(roomIndex < 0 && i < rooms.size()) {
                if(room.getRoomInfo().equals(rooms.get(i).getRoomInfo())) {
                    roomIndex = i;
                }
                i++;
            }

            if( roomIndex < 0) {
                rooms.add(room);
                roomIndex = rooms.size()-1; //minus one because index starts at 0
            }

            if(departmentBuildingIndex < 0) {
                dept.addBuilding(buildings.get(buildingIndex));
                //System.out.println("building count: " + dept.getBuildingList().size());
                departmentBuildingIndex = dept.getSizeofBuildingList() - 1;
            }

            Building selectedBuilding = dept.getBuilding(departmentBuildingIndex);
            selectedBuilding.insertRoom(rooms.get(roomIndex));
            dept.addRoom(rooms.get(roomIndex));
        }
        else {
            System.out.println("Error in University: unable to find building while inserting room: " + room.getRoomInfo());
        }
    }

    public ArrayList<Building> getBuildingList() { return buildings;}

    public ArrayList<Course> getCourseList() { return courses; }

    public ArrayList<Instructor> getInstructorList() { return instructors; }

    public ArrayList<Room> getRoomList() { return rooms; }
}

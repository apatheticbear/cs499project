package CourseScheduler.UniversityParts;


import java.util.ArrayList;
import java.util.Comparator;

/*
 *  Created by Zach Parker
 *  Modified by Hayley Johnsey
 *  This class represents a professor and helps manage the departments and courses he is involved in.
 */

public class Instructor {

    String preference;
    String fullName;
    ArrayList<String> departments = new ArrayList(5);
    ArrayList<Class> classes = new ArrayList(5);
    ArrayList<Course> courses = new ArrayList(5);
    ArrayList<ArrayList<Integer>> teachingTimes = new ArrayList<ArrayList<Integer>>();
    private int id;

    public Instructor(String departmentName, String firstName, String lastName, String preference){
        this.fullName = firstName + " " + lastName;
        this.departments.add(departmentName);
        this.preference = preference;

        //Set all teaching times to -1 to show that the instructor has no times they are teaching
        this.teachingTimes.add(new ArrayList<Integer>());
        this.teachingTimes.add(new ArrayList<Integer>());
        this.teachingTimes.add(new ArrayList<Integer>());

        for(int i = 0; i < this.teachingTimes.size(); i++)
        {
            for(int j = 0; j < 7; j++)
            {
                this.teachingTimes.get(i).add(-1);
            }
        }
    }

    public void setFullName(String fullName){
        this.fullName = fullName;
    }

    public String getFirstName(){
        String[] profName = this.fullName.split(" ");
        return profName[0];
    }

    public String getLastName(){
        String[] profName = this.fullName.split(" ");
        return profName[1];
    }

    public void setPreference(String preference){
        this.preference = preference;
    }

    public String getPreference(){
        return preference;
    }

    public void addCourse(Course course){
        this.courses.add(course);
    }

    public ArrayList<Course> getCourseList(){
        return this.courses;
    }

    public void addDepartment(String department){
        this.departments.add(department);
    }

    public ArrayList<String> getDepartments(){
        return this.departments;
    }

    //Compare instructor on their last and first name
    public static Comparator<Instructor> InstrNameComparator = new Comparator<Instructor>() {

        public int compare(Instructor i1, Instructor i2) {
            String instructorName1 = i1.getLastName().toUpperCase()+ " " + i1.getFirstName().toUpperCase();
            String instructorName2 = i2.getLastName().toUpperCase()+ " " + i2.getFirstName().toUpperCase();

            return instructorName1.compareTo(instructorName2);
        }
    };

    public void addTeachingTime(int day, int timePeriod)
    {
        //this.teachingTimes.get(day).add(timePeriod);
        this.teachingTimes.get(day).set(timePeriod, 1);
    }

    public ArrayList<ArrayList<Integer>> getTeachingTimes()
    {
        return this.teachingTimes;
    }
    
    public void setId(int newId) {
        id = newId;
    }
    
    public int getId() {
        return id;
    }

}

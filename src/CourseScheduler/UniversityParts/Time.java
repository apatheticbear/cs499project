package CourseScheduler.UniversityParts;

/**
 *
 * @author Zach
 */
public class Time {
    //all in minutes
    public int periodLength = 80;
    public int periodBreak = 20;
    public int dayStart = 480; //8:00am
    public int dayEnd = 1160; //7:20pm
    public enum time {
        Morning1, //8:00-9:20 morning
        Morning2, //9:40-11:00 morning
        Midday1,  //11:20-12:40 mid-day
        Midday2,  //1:00-2:20 mid-day
        Midday3,  //2:40-4:00 mid-day
        Evening1, //4:20-5:40 evening
        Evening2; //6:00-7:20 evening
    }
}

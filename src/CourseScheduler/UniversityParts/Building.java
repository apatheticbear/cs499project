package CourseScheduler.UniversityParts;

import java.util.ArrayList;

/**
 * Modified by Tristan Davis (02/12/19) (Who created this?) Maybe me (Whose me?)
 * -- (02/18/19) Added function to derive the abbreviated name from the full building name.
 * This class represent a building and manages a list of rooms within it as well as a list of courses
 */
public class Building
{
    private String buildingName;
    private ArrayList<Room> roomList;
    private ArrayList<Course> courses;
    private int id;

    public Building(String buildingName)
    {
        roomList = new ArrayList();
        courses = new ArrayList();
        this.buildingName = buildingName;
    }

    public String getBuildingName()
    {
        return this.buildingName;
    }

    public String getBuildingShortName() {
        //Derive short name from full name
        String shortName = this.buildingName.substring(0, Math.min(this.buildingName.length(), 3));
        return shortName.toUpperCase();
    }

    //Insert a room into the building
    public void insertRoom(Room newRoom)
    {

       boolean roomFound = false;
       int roomIndex = 0;
       //Search for the room: If a room is NOT found, add it to the list of rooms.
       while(roomFound == false && roomIndex < this.roomList.size()) {
           //System.out.println(newRoom.getRoomInfo()+"=="+this.roomList.get(roomIndex).getRoomInfo());
           if(newRoom.getRoomInfo().equals(this.roomList.get(roomIndex).getRoomInfo())) {
               //newRoom = this.roomList.get(roomIndex);
               roomFound = true;
           }
           roomIndex++;
       }
       //If the room is not found add it to the buildings list of rooms.
       if(roomFound == false) {
          this.roomList.add(newRoom);
       }
    }

    //Find a room from a given string, returns null if not found
    public Room findRoom(String roomName) {
        Room room = null;
        boolean roomFound = false;
        int roomIndex = 0;
        while(roomFound == false && roomIndex < this.roomList.size()) {
            if(this.roomList.get(roomIndex).getRoomNum().equals(roomName)) {
                room = this.roomList.get(roomIndex);
            }
            roomIndex++;
        }
        return room;
    }


    //Get the list of rooms
    public ArrayList<Room> getRoomList()
    {
        return this.roomList;
    }

    //Add a course to the building
    public void addCourse(Course c) {
        courses.add(c);
    }

    //Print room info to console for testing purposes
    public void printRoomListInfo()
    {
        roomList.forEach((room) -> {
            System.out.println(room.getRoomInfo());
        });
    }
    
    public void setId(int newId) {
        id = newId;
    }
    
    public int getId() {
        return id;
    }

}

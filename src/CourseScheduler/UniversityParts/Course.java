package CourseScheduler.UniversityParts;

import java.util.ArrayList;
import java.util.Comparator;

/*
 *  Created by Zach Parker
 *  Modified by Hayley Johnsey
 *  Modified by Tristan Davis
 *  --(02/18/19) Created getRoomInfo function to return room in BLD ### format.
 */

public class Course {

    private String className;
    private String professorFullName;
    private String roomNum;
    private String buildingName;
    private String classAndSection;
    private String preferredRoomNum;
    private String preferredBuilding;
    private String departmentName;
    private ArrayList<Day.day> daysArray;
    private TimePeriod timePeriod;
    private int expectedEnrollment;
    private int capacity;

    private String days;
    private Boolean isClassroomReq;
    private Boolean conflict;
    private ArrayList<Integer> preferredTimePeriods;
    private int preferredRoomID;
    private int sectionNumber = 1;
    private Boolean assigned;

    private int assignedTimePeriod;
    private int assignedDay;
    private Room assignedRoom;
    int id;

    public Course(){
        this.daysArray = new ArrayList<Day.day>();
        this.timePeriod = new TimePeriod();
        this.days = "";
        this.conflict = false;
        this.className = "";
        this.professorFullName = "";
        this.roomNum = "";
        this.buildingName = "";
        this.departmentName = "";
        this.isClassroomReq = false;
        this.assigned = false;
        this.assignedTimePeriod = -1;
        this.assignedDay = -1;
        this.assignedRoom = null;
        this.preferredBuilding = "";
        this.preferredRoomNum = "";
        this.expectedEnrollment = 0;
        this.capacity = 0;
    }

    public Course(String deptName, String clsName, String profFirstName, String profLastName){
        this.daysArray = new ArrayList<Day.day>();
        this.timePeriod = new TimePeriod();
        this.days = "";
        this.conflict = false;
        this.className = clsName;
        this.professorFullName =  profFirstName + " " + profLastName;
        this.roomNum = "";
        this.buildingName = "";
        this.departmentName = deptName;
        this.isClassroomReq = false;
        this.assigned = false;
        this.assignedTimePeriod = -1;
        this.assignedDay = -1;
        this.assignedRoom = null;
        this.preferredBuilding = "";
        this.preferredRoomNum = "";
        this.expectedEnrollment = 0;
        this.capacity = 0;
    }
    
    public Course(String deptName, String clsName, String profFirstName, String profLastName, String roomNum){
        this.daysArray = new ArrayList<Day.day>();
        this.timePeriod = new TimePeriod();
        this.days = "";
        this.conflict = false;
        this.className = clsName;
        this.professorFullName =  profFirstName + " " + profLastName;
        this.roomNum = roomNum;
        this.buildingName = "";
        this.departmentName = deptName;
        this.isClassroomReq = false;
        this.assigned = false;
        this.assignedTimePeriod = -1;
        this.assignedDay = -1;
        this.assignedRoom = null;
        this.preferredBuilding = "";
        this.preferredRoomNum = "";
        this.expectedEnrollment = 0;
        this.capacity = 0;
    }
    
    public int getCapacity(){
        return this.capacity;
    }
    
    public void setCapacity(int capacity){
        this.capacity = capacity;
    }
    
    public int getExpectedEnrollment(){
        return this.expectedEnrollment;
    }
    
    public void setExpectedEnrollment(int expectedEnrollment){
        this.expectedEnrollment = expectedEnrollment;
    }

    //gets the name of the class
    public String getClassName(){
        return this.className;
    }

    public String getClassAndSection() {
        classAndSection = this.className + "-" + this.getSectionNumber();
        return classAndSection;
    }

    //sets the name of the class
    public void setClassName(String className){
        this.className = className;
    }

    //gets the professor's first name
    public String getProfessorFirstName(){
        String[] profName = this.professorFullName.split(" ");
        return profName[0];
    }

    //gets the professor's last name
    public String getProfessorLastName() {
        String[] profName = this.professorFullName.split(" ");
        return profName[1];
    }

    //sets the whole name of the professor
    public void setProfessorFullName(String fullName){
        this.professorFullName = fullName;
    }

    public String getProfessorFullName(){
        return this.professorFullName;
    }

    //gets the room number
    public String getRoomNum(){
        return this.roomNum;
    }

    //sets the room number
    public void setRoomNum(String roomNum){
        this.roomNum = roomNum;
    }

    //gets the building name
    public String getBuildingName(){
        return this.buildingName;
    }

    //sets the building name
    public void setBuildingName(String buildingName){
        this.buildingName = buildingName;
    }

    //Creates room info in BLD ### format
    public String getRoomInfo()
    {
        return this.buildingName + " " + this.roomNum;
    }

    //gets the department name
    public String getDepartmentName(){
        return this.departmentName;
    }

    //sets the department name
    public void setDepartmentName(String departmentName){
        this.departmentName = departmentName;
    }

    //gets the class time
    public TimePeriod getTimePeriod() {
        return this.timePeriod;
    }

    //sets the class time
    public void setTimePeriod(TimePeriod classTime) {
        this.timePeriod = classTime;
    }

    //gets the string version of class time
    public String getStringClassTime(){
        return this.timePeriod.getTimePeriod();
    }

    //sets the string version of class time
    public void setStringClassTime(String stringClassTime){
        //Convert the time period string to a number
        timePeriod.setPeriodNumber(timePeriod.timeStringToInt(stringClassTime));
        timePeriod.setTimePeriod(stringClassTime);
        //this.stringClassTime = stringClassTime;
    }

    public void setIntClassTime(int intClassTime) {
        timePeriod.setPeriodNumber(intClassTime);
        //this.stringClassTime = timePeriod.getTimePeriod();
    }

    //gets the class day
    public ArrayList<Day.day> getDaysArray() {

        return this.daysArray;
    }

    public void addDay(Day.day aDay) {
        String dayList = this.getDays();
        dayList += aDay.toString();
        this.days = dayList;
        this.daysArray.add(aDay);
    }

    
    public void addDays(Day.day[] days) {
        for(int i = 0; i < days.length; i++) {
            String dayList = this.getDays();
            dayList += days[i].toString();
            this.days = dayList;
            this.daysArray.add(days[i]);
        }
    }

    /**
     * Use this one and just use a string to set it. Easiest way
     * @param days
     * @throws IllegalArgumentException
     */
    public void setDaysArray(String days) throws IllegalArgumentException {
        days = days.toUpperCase();
        this.daysArray = new ArrayList<Day.day>();
        this.days = "";
        String dayList = "";

        for(int i = 0; i < days.length(); i++) {
            char d = days.charAt(i);
            dayList += String.valueOf(d);
            this.days = dayList;
            Day.day newDay = Day.day.valueOf(String.valueOf(d));
            this.daysArray.add(newDay);
        }

    }

    public String getDays() {
        return this.days;
    }

    public int getDayNumber() {
        int dayNum = 0;
        if(days == "MW") {
            dayNum = 0;
        }
        else if(days == "TR") {
            dayNum = 1;
        }
        return dayNum;
    }


    //gets the required classroom for the course
    public Boolean getReqClassroom(){
        return this.isClassroomReq;
    }

    //sets the required classroom for the course
    public void setReqClassroom(Boolean reqClassroom){
        this.isClassroomReq = reqClassroom;
    }


    public static Comparator<Course> CourseNameComparator = (Course c1, Course c2) -> {
        String courseName1 = c1.getClassName().toUpperCase();
        String courseName2 = c2.getClassName().toUpperCase();

        return courseName1.compareTo(courseName2);
    };

    @Override
    public String toString() {
        return className + " " + roomNum + " " + buildingName + " " + departmentName + " " + getDays() + " " + getStringClassTime();
    }

    public Boolean getConflict() {
        return conflict;
    }

    public void setConflict(Boolean conflict) {
        this.conflict = conflict;
    }

    public void setPreferredTimePeriod(ArrayList<Integer> timePeriods)
    {
        this.preferredTimePeriods = new ArrayList<>();
        this.preferredTimePeriods = timePeriods;
    }

    public ArrayList<Integer> getPreferredTimePeriod()
    {
        return this.preferredTimePeriods;
    }

    public void setPreferredRoomID(int preferredRoomID)
    {
        this.preferredRoomID = preferredRoomID;
    }

    public int getPreferredRoomID()
    {
        return this.preferredRoomID;
    }

    public void setAssigned(boolean assigned)
    {
        this.assigned = assigned;
    }

    public boolean isAssigned()
    {
        return this.assigned;
    }

    public int getSectionNumber() {
        return sectionNumber;
    }

    public void setSectionNumber(int sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public void setDayAndTimePeriod(int incomingDay, int incomingTimePeriod)
    {
        this.assignedDay = incomingDay;
        this.assignedTimePeriod = incomingTimePeriod;
        this.setTimePeriod(timePeriod);

        if(incomingDay == 0)
        {
            this.setDaysArray("MW");
        }
        else if(incomingDay == 1)
        {
            this.setDaysArray("TR");
        }
        else if(incomingDay == 2)
        {
            this.setDaysArray("FR");
        }

        timePeriod.setPeriodNumber(incomingTimePeriod);


        //time periods
        //timePeriod.setPeriodNumber(incomingTimePeriod);
        //if(incomingTimePeriod == 0)
        //{
        //    this.stringClassTime = "8:00am-9:20am";
        //    timePeriod.setTimePeriod("8:00am-9:20am");
        //}
        //else if(incomingTimePeriod == 1)
        //{
        //    this.stringClassTime = ("9:40am-11:00am");
        //    timePeriod.setTimePeriod("9:40am-11:00am");
//
        //}
        //else if(incomingTimePeriod == 2)
        //{
        //    this.stringClassTime = ("11:20am-12:40pm");
        //    timePeriod.setTimePeriod("11:20am-12:40pm");
//
        //}
        //else if(incomingTimePeriod == 3)
        //{
        //    this.stringClassTime = ("1:00pm-2:20pm");
        //    timePeriod.setTimePeriod("1:00pm-2:20pm");
//
        //}
        //else if(incomingTimePeriod == 4)
        //{
        //    this.stringClassTime = ("2:40pm-4:00pm");
        //    timePeriod.setTimePeriod("1:00pm-2:20pm");
//
        //}
        //else if(incomingTimePeriod == 5)
        //{
        //    this.stringClassTime = ("4:20pm-5:40pm");
        //    timePeriod.setTimePeriod("4:20pm-5:40pm");
//
        //}
        //else if(incomingTimePeriod == 6)
        //{
        //    this.stringClassTime = ("6:00pm-7:20pm");
        //    timePeriod.setTimePeriod("6:00pm-7:20pm");
//
        //}
    }

    public void printDayAndTimePeriod()
    {
        String dayString = "";

        if(this.assignedDay == 0)
        {
            dayString = "Monday/Wednesday";
        }
        else if(this.assignedDay == 1)
        {
            dayString = "Tuesday/Thursday";
        }
        else if(this.assignedDay == 2)
        {
            dayString = "Friday";
        }
        else
        {
            dayString = "NO DAY ASSIGNED!!";
        }

        String timePeriodString = "";

        if(this.assignedTimePeriod == -1)
        {
            timePeriodString = "NO TIME PERIOD ASSIGNED!!";
        }
        else if(this.assignedTimePeriod == 0)
        {
            timePeriodString = "8:00am-9:20am";
        }
        else if(this.assignedTimePeriod == 1)
        {
            timePeriodString = "9:40am-11:00am";
        }
        else if(this.assignedTimePeriod == 2)
        {
            timePeriodString = "11:20am-12:40pm";
        }
        else if(this.assignedTimePeriod == 3)
        {
            timePeriodString = "1:00pm-2:20pm";
        }
        else if(this.assignedTimePeriod == 4)
        {
            timePeriodString = "2:40pm-4:00pm";
        }
        else if(this.assignedTimePeriod == 5)
        {
            timePeriodString = "4:20pm-5:40pm";
        }
        else if(this.assignedTimePeriod == 6)
        {
            timePeriodString = "6:00pm-7:20pm";
        }

        System.out.print(dayString + " -- " + timePeriodString);
    }

    public void setAssignedRoom(Room assignedRoom)
    {
        this.assignedRoom = assignedRoom;
        assignedRoom.setAvailable(false);
    }

    public void setAssignedRoomInDepartment(Room assignedRoom)
    {
        this.assignedRoom = assignedRoom;
    }

    public void printAssignedRoomInfo()
    {
        System.out.print(this.assignedRoom.getRoomInfo());
    }

    public int getAssignedTimePeriod()
    {
        return this.assignedTimePeriod;
    }

    public int getAssignedDay()
    {
        return this.assignedDay;
    }

    public Room getAssignedRoom()
    {
        return this.assignedRoom;
    }


    public String getPreferredRoomNum() {
        return preferredRoomNum;
    }

    public void setPreferredRoomNum(String preferredRoomNum) {
        this.preferredRoomNum = preferredRoomNum;
    }

    public String getPreferredBuilding() {
        return preferredBuilding;
    }

    public void setPreferredBuilding(String preferredBuilding) {
        this.preferredBuilding = preferredBuilding;
    }
    
    public void setId(int newId) {
        id = newId;
    }
    
    public int getId() {
        return id;
    }
}




package CourseScheduler.UniversityParts;

/**
 *
 * @author Zach
 */
public class TimePeriod {
    String timePeriod;
    String timeOfDay;
    int periodNumber;


    public TimePeriod(){
        this.timePeriod = "";
        this.timeOfDay = "";
    }

    public void setTimePeriod(String timePeriod){
        this.timePeriod = timePeriod;
        //this.periodNumber = timeStringToInt(timePeriod);
    }

    public String getTimePeriod(){
        return this.timePeriod;
    }

    //Converting string to number for period stuff
    public int timeStringToInt(String timePeriodString) {
        int periodNumber = 0;
        switch(timePeriodString) {
            case "8:00am-9:20am":
                periodNumber = 0;
                break;
            case "9:40am-11:00am":
                periodNumber = 1;
                break;
            case "11:20am-12:40pm":
                periodNumber = 2;
                break;
            case "1:00pm-2:20pm":
                periodNumber = 3;
                break;
            case "2:40pm-4:00pm":
                periodNumber = 4;
                break;
            case "4:20pm-5:40pm":
                periodNumber = 5;
                break;
            case "6:00pm-7:20pm":
                periodNumber = 6;
                break;
        }
        return periodNumber;

    }

    public String timeIntToString(int time) {
       String periodString = null;
       switch(time) {
           case 0:
               periodString = "8:00am-9:20am";
               break;
           case 1:
               periodString = "9:40am-11:00am";
               break;
           case 2:
               periodString = "11:20am-12:40pm";
               break;
           case 3:
               periodString = "1:00pm-2:20pm";
               break;
           case 4:
               periodString = "2:40pm-4:00pm";
               break;
           case 5:
               periodString = "4:20pm-5:40pm";
               break;
           case 6:
               periodString = "6:00pm-7:20pm";
               break;
           default:
               periodString = "Error in TimePeriod.java: 83: time period string not found";
       }
       return periodString;
    }

    public void setTimeOfDay(String timeOfDay){
        this.timeOfDay = timeOfDay;
    }

    public String getTimeOfDay(){
        return this.timeOfDay;
    }

    public int getPeriodNumber() {
        return this.periodNumber;
    }

    public void setPeriodNumber(int periodNumber) {
        this.periodNumber = periodNumber;
        this.timePeriod = timeIntToString(periodNumber);
    }
}

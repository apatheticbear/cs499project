package CourseScheduler.UniversityParts;

import CourseScheduler.GUI.RowRoom;

import java.util.ArrayList;


/*
    This class represents a room and maintains a list of courses and a matrix of available time slots for that room
 */

public class Room
{
    private String buildingName;
    private String roomNum;
    private int capacity;
    //these two will be the same size and each index corresponds to the other object at that index
    private ArrayList<Course> courseList;
    private ArrayList<Instructor> instructorList;
    
    private boolean isAvailable;
    private int id;
    private ArrayList<TimePeriod> timePeriods;
    private ArrayList<Day.day> days;
    private ArrayList<ArrayList<Boolean>> availabilityTable;
    private RowRoom assignedRow;

    //Construct a room without defined capacity
    public Room(String buildingName, String roomNum, ArrayList<TimePeriod> tp, ArrayList<Day.day> day)
    {
        this.buildingName = buildingName;
        this.roomNum = roomNum;
        //this.isAvailable = true;
        this.courseList = new ArrayList<>();
        this.capacity = 0;
        this.timePeriods = tp;
        this.days = day;
        assignedRow = null;

        //create an availability table
        for(int i = 0; i < days.size(); i++) {
            availabilityTable.add(new ArrayList<Boolean>());
            for(int j = 0; j < timePeriods.size(); j++){
                availabilityTable.get(i).add(true);
            }
        }
    }

    //Define a room with a defined capacity
    public Room(String buildingName, String roomNum, int capacity, ArrayList<TimePeriod> tp, ArrayList<Day.day> day){
        this.buildingName = buildingName;
        this.roomNum = roomNum;
        //this.isAvailable = true;
        this.courseList = new ArrayList<>();
        this.capacity = capacity;
        this.timePeriods = tp;
        this.days = day;
        availabilityTable = new ArrayList<ArrayList<Boolean>>();
        assignedRow = null;

        //System.out.println("Days: " + days.size());
        //System.out.println("Tps: " + timePeriods.size());
        for(int i = 0; i < days.size(); i++) {
            availabilityTable.add(new ArrayList<Boolean>());
            for(int j = 0; j < timePeriods.size(); j++){
                availabilityTable.get(i).add(true);
            }
        }


    }



    public void setCapacity(int cap){
        this.capacity = cap;
    }

    public boolean getAvailability(int day, int period) {
        boolean found = false;
        found = availabilityTable.get(day).get(period);
        return found;
    }

    public void setAvailability(int day, int period, boolean available) {
        availabilityTable.get(day).set(period, available); //available;
    }

    public int getCapacity(){
        return this.capacity;
    }

    public String getRoomNum(){
        return this.roomNum;
    }

    public String getBuildingName(){
        return this.buildingName;
    }

    public String getRoomInfo()
    {
        return this.buildingName + " " + this.roomNum;
    }


    //Add a course to the room
    public void addCourse(Course newCourse, int day, int period)
    {
        //System.out.println(this.getRoomInfo() + "Add course" + newCourse.getClassAndSection());
        setAvailability(day, period, false);
        this.courseList.add(newCourse);


    }

    //Return the number of courses currently assigned to a room
    //This value should be either 0 or 1. If it's higher, that means there's a conflict that needs to be resolved
    public int getNumCoursesInRoom()
    {
        return this.courseList.size();
    }

    //Return the entire arrayList of rooms
    public ArrayList<Course> getCourseList()
    {
        return courseList;
    }

    public void printFullRoomData()
    {
        System.out.print(this.buildingName + " " + this.roomNum + "\t\tAVAILABLE: ");// + this.isAvailable +"\t\t\tCLASSES ---->    ");
        for(int i = 0; i < this.courseList.size(); i++)
        {
            if(this.courseList.get(i).isAssigned() == true)
            {
                System.out.print(" " + this.courseList.get(i).getClassName() + " " + this.courseList.get(i).getProfessorFullName());
            }
        }
    }


    public boolean getAvailability()
    {
        return this.isAvailable;
    }

    public void setAvailable(boolean isAvailable)
    {
        this.isAvailable = isAvailable;
    }
    
    public void setId(int newId) {
        id = newId;
    }
    
    public int getId() {
        return id;
    }

    //Rooms must be cleared at certain points to prevent duplication
    public void clearCourseList() {
        this.courseList = new ArrayList<Course>();
    }

}

package CourseScheduler.UniversityParts;

import java.util.*;

/*
 *  Created by Zach Parker
 *  Modified by Hayley Johnsey
 *  Modified by Tristan Davis (02/12/19)
 *  -- (02/18/19) fixed logic errors in adding courses
 */

public class Department {

    String departmentName;
    ArrayList<Building> buildings = new ArrayList(5);
    ArrayList<Course> courses = new ArrayList(100);
    ArrayList<Instructor> instructors = new ArrayList(100);
    ArrayList<String> coursesOffered = new ArrayList(200); //not courses being offered this semester
    ArrayList<Room> roomList = new ArrayList(100);
    ArrayList<TimePeriod> timePeriods = new ArrayList<>();
    int id;
    
    public Department(){

    }

    public void setDepartmentName(String departmentName){
        this.departmentName = departmentName;
    }

    public String getDepartmentName(){
        return this.departmentName;
    }

    public void addCourse(Course c){

        courses.add(c);
        //Search instructor list for instructor to add class to
        boolean instructorFound = false; // Set found boolean to false
        int i = 0; //Set iterator
        while((i < instructors.size()) && (instructorFound == false)){
            Instructor instructor = instructors.get(i);

            if(instructor.getFirstName().equalsIgnoreCase(c.getProfessorFirstName()) && instructor.getLastName().equalsIgnoreCase((c.getProfessorLastName()))) {
                instructor.addCourse(c);
                instructorFound = true;
            }
            i++;
        }
        if((i >= instructors.size()) &&  instructorFound == false) {
            System.out.println("Department.java: Unable to find professor: " + c.getProfessorFullName() + " Of class: " + c.getClassName());
        }

        //Search for building in list of buildings to add class to
        boolean buildingFound = false;
        i = 0; //Set iterator
        //If the building name of the class is an empty string then it is not assigned to any building
        if(!c.getBuildingName().equals("")) {
            while((i < buildings.size()) && (buildingFound == false)){
                Building building = buildings.get(i);
                if(building.getBuildingShortName().equals(c.getBuildingName())) {
                    building.addCourse(c);
                    buildingFound = true;
                }
                i++;
            }
            if((i >= buildings.size()) &&  buildingFound == false) {
                System.out.println("Department.java: Unable to find building: " + c.getBuildingName() + " Of class: " + c.getClassName());
            }
        }
        boolean roomFound = false;
        i = 0; //Set iterator
        if(!c.getRoomInfo().equals("")) {
            while((i < roomList.size()) && (roomFound == false)){
                Room rm = roomList.get(i);
                if(rm.getRoomInfo().equals(c.getRoomInfo())) {
                    rm.addCourse(c, c.getAssignedDay(), c.getAssignedTimePeriod());
                    roomFound = true;
                }
                i++;
            }
            /*
            if((i >= roomList.size()) &&  roomFound == false) {
                //System.out.println("Department.java: Unable to find room: " + c.getRoomInfo() + " Of class: " + c.getClassName());
            }*/
        }
    }

    //removes a course from the courses ArrayList
    public void deleteCourse(Course c) {
        courses.remove(c);
    }

    //Ads an instructor to the instructors ArrayList
    public void addInstructor(Instructor instructor){
        instructors.add(instructor);
    }

    //Adds a building to the ArrayList buildings
    public void addBuilding(Building building){
        //System.out.println(building.getBuildingShortName());
        //Make sure duplicate buildings are not being added.
        boolean buildingFound = false;
        int buildingIndex = 0;
        //Search for buildings in building list
        while(buildingFound == false && buildingIndex < this.buildings.size()) {
            //If a building is found check to see if a new room needs to be added to that building
            //System.out.println(this.buildings.get(buildingIndex).getBuildingShortName()+"=="+building.getBuildingShortName());
            if(this.buildings.get(buildingIndex).getBuildingShortName().equals(building.getBuildingShortName())) {
                buildingFound = true;
                for(Room room : building.getRoomList()) {
                    boolean roomFound = false;
                    int roomIndex = 0;
                    //Search for the room: If a room is NOT found, add it to the list of rooms.
                    while(roomFound == false && roomIndex < this.buildings.get(buildingIndex).getRoomList().size()) {
                        System.out.println(room.getRoomInfo()+"=="+this.buildings.get(buildingIndex).getRoomList().get(roomIndex).getRoomInfo());
                        if(room.getRoomInfo().equals(this.buildings.get(buildingIndex).getRoomList().get(roomIndex).getRoomInfo())) {
                            roomFound = true;
                        }
                        roomIndex++;
                    }
                    //If the room is not found add it to the buildings list of rooms.
                    if(roomFound == false) {
                        this.buildings.get(buildingIndex).getRoomList().add(room);
                    }
                }
            }
            buildingIndex++;
        }
        //If the building does not already exist add it to the list of buildings.
        if(buildingFound == false) {
            buildings.add(building);
        }
    }

    //Add a room to the department
    public void addRoom(Room newRoom)
    {
        roomList.add(newRoom);
    }

    public ArrayList<Room> getRoomList()
    {
        return this.roomList;
    }

    //sorts the instructors
    public void sortInstructors(String departmentName) {
        Collections.sort(instructors, Instructor.InstrNameComparator);
    }

    //sorts the courses
    public void sortCourses(String departmentName) {
        Collections.sort(courses, Course.CourseNameComparator);
    }

    //gets an instructor at a specified index
    public Instructor getInstructor(int index) {
        return instructors.get(index);
    }

    //returns an instructor object given a first and last name if found
    //returns null if not found
    public Instructor getInstructorByName(String firstName, String lastName){
        Instructor instructor = null;
        for(int i = 0; i < this.instructors.size(); i++){
            if(this.instructors.get(i).getFirstName().equals(firstName) && this.instructors.get(i).getLastName().equals(lastName)){
                instructor = this.instructors.get(i);
            }
        }
        return instructor;
    }

    //Return list of instructors in this department
    public ArrayList<Instructor> getInstructorList() { return instructors; }

    //gets the course at a specified index
    public Course getCourse(int index) {
        return courses.get(index);
    }

    // Return the whole list of courses
    public ArrayList<Course> getCourseList(){
        return courses;
    }

    public Building getBuilding(int index) {
        return buildings.get(index);
    }

    public int getSizeofCourseList() {
        int size = courses.size();
        return size;
    }

    public int getSizeofBuildingList() {
        int size = buildings.size();
        return size;
    }

    // Return the whole list of courses
    public ArrayList<Building> getBuildingList(){
        return buildings;
    }

    public int getSizeofInstructorsList() {
        int size = instructors.size();
        return size;
    }

    //adds an offered course by the department
    public void addCourseOffered(String courseOffered){
        this.coursesOffered.add(courseOffered);
    }

    public void setDeptTimePeriods(ArrayList<TimePeriod> periods)
    {
        this.timePeriods = periods;
    }

    public ArrayList<TimePeriod> getDeptTimePeriods()
    {
        return this.timePeriods;
    }

    public int findBuilding(String buildingName) {
        int foundIndex = -1;
        int i = 0;
        while(foundIndex < 0 && i < buildings.size())
        {
            if(buildings.get(i).getBuildingShortName().equals(buildingName)) {
                foundIndex = i;
            }
            i++;
        }
        if(foundIndex > -1) {
            return foundIndex;
        }
        else{
            return -1;
        }

    }
    
    public void setId(int newId) {
        id = newId;
    }
    
    public int getId() {
        return id;
    }

}

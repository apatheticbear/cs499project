package CourseScheduler.MVC;

import CourseScheduler.Exceptions.DepartmentException;
import CourseScheduler.Exceptions.FileParserException;
import CourseScheduler.UniversityParts.*;
import CourseScheduler.FileLoader.FileReaderContext;
import CourseScheduler.FileLoader.CSVFileReader;
import CourseScheduler.GUI.ScheduleGrid;


import java.util.ArrayList;

/**
 *
 * @author Zach
 */
public final class SchedulingSystemModel {

    private SchedulingSystemModel() {
    }

    public static SchedulingSystemModel getInstance() {
        if (ssm == null) {
            ssm = new SchedulingSystemModel();
        }
        return ssm;
    }

    //This will take in a list of file paths to the files to be opened and store them here in the model
    public void setFilePaths(ArrayList<String> filePathsss) {
        for (int i = 0; i < filePathsss.size(); i++) {
            ssm.filePaths.add(filePathsss.get(i));
        }
    }

    //return the list of filePaths
    public ArrayList<String> getFilePaths() {
        return filePaths;
    }

    //will wipe out the current list of file paths
    public void clearFilePaths() {
        filePaths = new ArrayList(20);
    }

    //test method to make sure the files paths are properly set
    public void printFilePaths() {
        //test code to make sure it is recieved by the model properly
        for (int i = 0; i < ssm.filePaths.size(); i++) {
            System.out.println(ssm.filePaths.get(i));
        }
    }

    //If any necessary course data is missing, then this will just add values
    public void populateEmptyCourseData()
    {
        if(!alreadyScheduled) {
            ScheduleGrid grid = new ScheduleGrid(ssm.uni.getTimePeriods(), ssm.uni);
            grid.populateCellsWithRooms(); //puts every room into every timeperiod of every day
            grid.assignInstructorPreferences();// assigns an arraylist of timeperiods to each instructor based on time preferences
            grid.assignRoomPreferences();
            grid.schedule();
            alreadyScheduled = true;
        } else {
            //If schedule was already created we need to get rid of the old schedule
            for(Department dept : ssm.uni.getDepts()) {
                for (Room room : dept.getRoomList()) {
                    room.clearCourseList();
                }
            }
            ScheduleGrid grid = new ScheduleGrid(ssm.uni.getTimePeriods(), ssm.uni);
            grid.populateCellsWithRooms(); //puts every room into every timeperiod of every day
            grid.assignInstructorPreferences();// assigns an arraylist of timeperiods to each instructor based on time preferences
            grid.assignRoomPreferences();
            grid.schedule();
        }
    }

    //creates a file parser to parse the file and get instructors and classes back
    public void parseDataFiles() throws FileParserException{
        for (int i = 0; i < filePaths.size(); i++) {
            try{
                FileReaderContext frc = new FileReaderContext(new CSVFileReader(filePaths.get(i), uni));
                frc.readFile();

                frc.getDepartment();
                if(uni.getTimePeriods().isEmpty()){
                    ssm.uni.setTimePeriods(frc.getTimePeriods());
                }
                if (ssm.uni.getTimeBetweenPeriods() <= 0) {
                    ssm.uni.setTimeBetweenPeriods(frc.getTimeBetweenPeriods());
                }
            }
            catch(DepartmentException d){
                
            }
        }
    }

    //returns the array list of department objects
    public ArrayList<Department> getDepartmentList() {
        return ssm.uni.getDepts();
    }

    public University getUniversity() {
        return ssm.uni;
    }

    public ArrayList<TimePeriod> getTimePeriods() {
        return ssm.uni.getTimePeriods();
    }
    
    public void clearUniversity(){
        this.uni = new University();
    }
    

    private static SchedulingSystemModel ssm = null;
    private Boolean alreadyScheduled= false;
    private University uni = new University();
    private ArrayList<String> filePaths = new ArrayList(20);
}

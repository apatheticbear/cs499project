package CourseScheduler.MVC;

import CourseScheduler.GUI.FileLoaderGUI;

/**
 *
 * @author Zach
 */
public class SchedulingSystemController {
    
    public SchedulingSystemController(String[] args){
        SchedulingSystemModel ssm = SchedulingSystemModel.getInstance();
        FileLoaderGUI flg = new FileLoaderGUI();
        flg.initialize(ssm);
        flg.FileLoaderGUIStart(args);
    }
    
}

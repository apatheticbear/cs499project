package CourseScheduler.FileLoader;

import CourseScheduler.Exceptions.DepartmentException;
import CourseScheduler.Exceptions.FileParserException;
import CourseScheduler.UniversityParts.Department;
import CourseScheduler.UniversityParts.TimePeriod;
import java.util.ArrayList;

/**
 *
 * @author Zach
 */
public class FileReaderContext {
    
    public FileReaderContext(FileReaderInterface fileReader){
        this.fileReader = fileReader;
    }
    
    public void readFile() throws FileParserException, DepartmentException{
        fileReader.readFile();
    }
    
    public ArrayList<TimePeriod> getTimePeriods(){
        return this.fileReader.getTimePeriods();
    }
    
    public int getTimeBetweenPeriods(){
        return this.fileReader.getTimeBetweenPeriods();
    }
    
    //returns the department object made by parsing
    public Department getDepartment(){
        return this.fileReader.getDepartment();
    }
    
    private final FileReaderInterface fileReader;
}

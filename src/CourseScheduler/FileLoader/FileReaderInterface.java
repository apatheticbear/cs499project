package CourseScheduler.FileLoader;

import CourseScheduler.Exceptions.DepartmentException;
import CourseScheduler.Exceptions.FileParserException;
import CourseScheduler.UniversityParts.Department;
import CourseScheduler.UniversityParts.TimePeriod;
import java.util.ArrayList;



/**
 *
 * @author Zach
 */
public interface FileReaderInterface {
    public void readFile() throws FileParserException, DepartmentException;
    public ArrayList<TimePeriod> getTimePeriods();
    public int getTimeBetweenPeriods();
    public Department getDepartment();
}

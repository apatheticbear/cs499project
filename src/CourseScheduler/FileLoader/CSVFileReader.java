package CourseScheduler.FileLoader;

import CourseScheduler.Exceptions.DepartmentException;
import CourseScheduler.Exceptions.FileParserException;
import CourseScheduler.UniversityParts.Department;
import CourseScheduler.UniversityParts.Instructor;
import CourseScheduler.UniversityParts.Room;
import CourseScheduler.UniversityParts.Course;
import CourseScheduler.UniversityParts.Building;
import CourseScheduler.UniversityParts.TimePeriod;
import CourseScheduler.UniversityParts.University;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Edited by Tristan Davis -- (02/18/19) Changed order of adding data to the
 * department The way Department.java is written courses must be added to the
 * dept last. Also updated to use getDepartmentShortName.
 *
 * @author Zach
 *
 */
public class CSVFileReader implements FileReaderInterface {

    public CSVFileReader(String filePath, University uni) {
        this.uni = uni;
        this.file = new File(filePath);
        try {
            this.scanner = new Scanner(file);
        } catch (FileNotFoundException fnf) {
            System.out.println("Error reading file: " + filePath);
        }
    }

    //reads the input of the correctly formatted csv file
    /**
     *
     * @throws CourseScheduler.Exceptions.FileParserException
     * @throws CourseScheduler.Exceptions.DepartmentException
     */
    @Override
    public void readFile() throws FileParserException, DepartmentException {

        String departmentName = "";
        this.department = new Department();
        String currentLine;

        while (hasNextLine()) {
            currentLine = this.getNextLine().trim();

            if (currentLine.equalsIgnoreCase("Time Periods:")) {
                
                //there are no time periods, then you can parse this
                if(this.uni.getTimePeriods().isEmpty()){
                    currentLine = this.getNextLine().replaceAll("\\s+", ""); //gets the line and strips the white space out of it
                    int periodCount = 0;
                    while (currentLine.equalsIgnoreCase("") != true) {
                        //parse the line
                        String[] parsedLine = this.parseLine(currentLine); //gets the parsed line back in an array
                        this.createTimePeriod(parsedLine[0].trim(), parsedLine[1].trim(), periodCount);
                        currentLine = this.getNextLine();
                        periodCount++;
                    }
                    this.uni.setTimePeriods(this.timePeriods);
                }
            } else if (currentLine.equalsIgnoreCase("Time Between Periods:")) {
                currentLine = this.getNextLine().replaceAll("\\s+", ""); //gets the line and strips the white space out of it
                if(isInteger(currentLine)){
                    //checks if there is a time between time periods already or not
                    if(this.uni.getTimeBetweenPeriods() <= 0){
                        this.timeBetweenClasses = Integer.parseInt(currentLine); //get the integer number
                    }
                }
                else{
                    throw new FileParserException("Error with Time Between Periods: portion of " + departmentName + " CSV file. \n   You need to have an integer! \n");
                }
            } else if (currentLine.equalsIgnoreCase("Department:")) {
                currentLine = this.getNextLine().replaceAll("\\s+", ""); //gets the line and strips the white space out of it;
                departmentName = currentLine;
                for(int i = 0; i < uni.getDepts().size(); i++){
                    if(departmentName.equalsIgnoreCase(uni.getDepts().get(i).getDepartmentName())){
                        throw new DepartmentException("Department " + departmentName + " in " + this.file.getPath() + " is already used. \n");
                    }
                }
                this.department.setDepartmentName(departmentName);
            } else if (currentLine.equalsIgnoreCase("Location:")) {
                //get line underneath location that contains building names
                currentLine = this.getNextLine().trim(); //gets the line and takes leading and trailing whitespace from it
                //parse the line
                String[] parsedLine = this.parseLine(currentLine); //gets the parsed line back in an array
                //put the building names in the buildings ArrayList
                for (int i = 0; i < parsedLine.length; i++) {
                    this.createBuilding(parsedLine[i].trim());
                }
            } else if (currentLine.equalsIgnoreCase("Courses Offered:")) {
                currentLine = this.getNextLine().trim(); //gets the line and takes leading and trailing whitespace from it
                while (currentLine.equalsIgnoreCase("") != true) {
                    String[] parsedLine = this.parseLine(currentLine); //gets the parsed line back in an array
                    //add the courses parsed from this line to the ArrayList
                    for (int i = 0; i < parsedLine.length; i++) {
                        this.department.addCourseOffered(parsedLine[i].trim());
                    }
                    //go to next Line
                    currentLine = this.getNextLine();
                }
            } else if (currentLine.equalsIgnoreCase("Classroom:")) {
                currentLine = this.getNextLine().trim(); //gets the line and takes leading and trailing whitespace from it
                while (currentLine.equalsIgnoreCase("") != true) {
                    String[] parsedLine = this.parseLine(currentLine); //gets the parsed line back in an array

                    //add the courses parsed from this line to the ArrayList
                    String[] courseInfo = parsedLine[0].trim().split("\\s+"); //trims leading and trailing whitespace and then splits it in the middle space
                    int numSeats = Integer.parseInt(parsedLine[1].trim()); //gets the number of seats
                    this.createRoom(courseInfo[0], courseInfo[1], numSeats, departmentName);
                    //go to next Line
                    currentLine = this.getNextLine();
                }
            } else if (currentLine.equals("Classroom Preferences:")) {
                currentLine = this.getNextLine().trim();
                while (currentLine.equalsIgnoreCase("") != true) {
                    String[] parsedLine = this.parseLine(currentLine); //gets the parsed line back in an array
                    if(parsedLine.length != 2){
                        throw new FileParserException("Error with Classroom Preference portion of " + departmentName + " CSV file. \n   You need a class and a room! \n");
                    }
                    //put the class preference in
                    this.classPreferences.add(parsedLine[0].trim());
                    //then put the room preference in
                    this.roomPreferences.add(parsedLine[1].trim());
                    currentLine = this.getNextLine();
                }
            } else if (currentLine.equals("Faculty Assignments:")) {
                currentLine = this.getNextLine().trim();
                //read until eof
                while (this.hasNextLine()) {
                    String[] parsedLine = this.parseLine(currentLine); //gets the parsed line back in an array
                    String firstName = "";
                    String lastName = "";
                    String classTimePref = "";
                    //go through the parsed data from the line
                    for (int i = 0; i < parsedLine.length; i++) {
                        //set instructor name
                        if (i == 0) {
                            //split the first and last name up
                            String[] instructorInfo = parsedLine[0].split("\\s+");
                            if (instructorInfo.length != 2) {
                                throw new FileParserException("Error with intstructor portion of " + departmentName + " CSV file. \n    Instructor doesn't have two names. \n");
                            }
                            firstName = instructorInfo[0].trim();
                            lastName = instructorInfo[1].trim();
                        } //last element if the preference of class time
                        else if (i == parsedLine.length - 1) {
                            //split the preference from the text "preference:"
                            String[] preference = parsedLine[i].trim().split("\\s+");
                            if(preference.length != 2){
                                throw new FileParserException("Error with intstructor portion of " + departmentName + " CSV file. \n    Instructor preference error \n");
                            }
                            classTimePref = preference[1].trim();
                        } //Create a course object for the courses to be taught
                        else {
                            
                            if(parsedLine[i].contains("Enrollment:")){
                                //splits on : with any number of spaces after
                                String[] enrollmentLine = parsedLine[i].replaceAll("\\s+", " ").trim().split("\\s+");
                                if(enrollmentLine.length != 4){
                                    throw new FileParserException("Error with course enrollment in instructor portion of " + departmentName + " CSV file. \n    Needs to be in format 'Course Number Enrollment: #' \n");
                                }
                                
                                if(isInteger(enrollmentLine[3])){
                                    int expEnrollment = Integer.parseInt(enrollmentLine[3]);
                                    this.createCourse(departmentName, enrollmentLine[0] + " " + enrollmentLine[1], firstName, lastName, expEnrollment);
                                }
                                else{
                                    throw new FileParserException("Error with Enrollment portion of " + departmentName + " in instructor "  + firstName + " " +lastName + " CSV file. \n   You need to have an integer! \n");
                                }
                                
                            }
                            else{
                                String courseName = parsedLine[i].replaceAll("\\s+", " ");
                                this.createCourse(departmentName, courseName.trim(), firstName, lastName);
                            }
                            
                        }
                    }
                    //create the instructor with given info
                    this.createInstructor(departmentName, firstName, lastName, classTimePref);
                    currentLine = this.getNextLine();
                }
                String[] parsedLine = this.parseLine(currentLine); //gets the parsed line back in an array
                String firstName = "";
                String lastName = "";
                String classTimePref = "";
                //go through the parsed data from the line
                for (int i = 0; i < parsedLine.length; i++) {
                    //set instructor name
                    if (i == 0) {
                        //split the first and last name up
                        String[] instructorInfo = parsedLine[0].split("\\s+");
                        if (instructorInfo.length != 2) {
                            throw new FileParserException("Error with intstructor portion of " + departmentName + " CSV file. \n    Instructor doesn't have two names. \n");
                        }
                        firstName = instructorInfo[0].trim();
                        lastName = instructorInfo[1].trim();
                    } //last element if the preference of class time
                    else if (i == parsedLine.length - 1) {
                        //split the preference from the text "preference:"
                        String[] preference = parsedLine[i].trim().split("\\s+");
                        if(preference.length != 2){
                                throw new FileParserException("Error with intstructor portion of " + departmentName + " CSV file. \n    Instructor preference error \n");
                        }
                        classTimePref = preference[1].trim();
                    } //Create a course object for the courses to be taught
                    else {
                        if(parsedLine[i].contains("Enrollment:")){
                                //splits on : with any number of spaces after
                                String[] enrollmentLine = parsedLine[i].replaceAll("\\s+", " ").trim().split("\\s+");
                                if(enrollmentLine.length != 4){
                                    throw new FileParserException("Error with course enrollment in instructor portion of " + departmentName + " CSV file. \n    Needs to be in format 'Course Number Enrollment: #' \n");
                                }
                                
                                if(isInteger(enrollmentLine[3])){
                                    int expEnrollment = Integer.parseInt(enrollmentLine[3]);
                                    this.createCourse(departmentName, enrollmentLine[0] + " " + enrollmentLine[1], firstName, lastName, expEnrollment);
                                }
                                else{
                                    throw new FileParserException("Error with Enrollment portion of " + departmentName + " in instructor "  + firstName + " " +lastName + " CSV file. \n   You need to have an integer! \n");
                                }
                                
                            }
                            else{
                                String courseName = parsedLine[i].replaceAll("\\s+", " ");
                                this.createCourse(departmentName, courseName.trim(), firstName, lastName);
                            }
                    }
                }
                //create the instructor with given info
                this.createInstructor(departmentName, firstName, lastName, classTimePref);
            }
        }

        this.addRequiredRooms();

        uni.addDepartment(this.department);

        //add the buildings to the department
        for (int i = 0; i < this.buildings.size(); i++) {
            //this.department.addBuilding(this.buildings.get(i));
            this.uni.addBuilding(departmentName, this.buildings.get(i));
        }

        //add the instructors to the department
        for (int i = 0; i < this.instructors.size(); i++) {
            //this.department.addInstructor(this.instructors.get(i));
            this.uni.addInstructor(departmentName, this.instructors.get(i));
        }

        //add the courses to the department
        for (int i = 0; i < this.courses.size(); i++) {
            //this.department.addCourse(this.courses.get(i));
            this.uni.addCourse(departmentName, this.courses.get(i));
        }

    }

    //Adds the required classroom to the course objects
    private void addRequiredRooms() {
        for (int i = 0; i < this.classPreferences.size(); i++) {
            for (int j = 0; j < this.courses.size(); j++) {
                //check each course
                if (this.classPreferences.get(i).equals(this.courses.get(j).getClassName())) {
                    //add the corresponding room number and building to this course
                    String[] courseInfo = this.roomPreferences.get(i).split(" ");
                    this.courses.get(j).setPreferredBuilding(courseInfo[0]);
                    this.courses.get(j).setPreferredRoomNum(courseInfo[1]);
                    this.courses.get(j).setReqClassroom(true);
                }
            }
        }
    }

    //creates a TimePeriod given the correct data and stores in in the array list in this class
    private void createTimePeriod(String time, String timeOfDay, int periodNumber) {
        TimePeriod timePeriod = new TimePeriod();
        timePeriod.setTimePeriod(time); //add the time of the class
        timePeriod.setTimeOfDay(timeOfDay); //add the part of the day it is in
        timePeriod.setPeriodNumber(periodNumber);
        //add to the array list
        this.timePeriods.add(timePeriod);
    }

    //creates a building with a name and stores it in the appropriate array list
    private void createBuilding(String buildingName) {
        Building building = new Building(buildingName);
        this.buildings.add(building);
    }

    //creates a room and puts it in the correct building and department
    private void createRoom(String buildingName, String classNum, int capacity, String departmentName) {

        Room room = null; //Create room

        int buildingIndex = this.uni.findBuilding(buildingName); //Find building
        if(buildingIndex > -1) { //If building is found
            Room temp = this.uni.getBuildingList().get(buildingIndex).findRoom(classNum);
            if(temp != null) {
                room = temp;
                //add it to the departments room listing as well
                this.department.addRoom(room);
            }
            else {
                room = new Room(buildingName, classNum, capacity, this.uni.getTimePeriods(), this.uni.getDays());
                //add it to the departments room listing as well
                this.department.addRoom(room);

                //add to university as well
                this.uni.addRoom(room);
            }
        }
        else {
            room = new Room(buildingName, classNum, capacity, this.uni.getTimePeriods(), this.uni.getDays());
            for (int j = 0; j < this.buildings.size(); j++) {
                if (buildingName.equals(this.buildings.get(j).getBuildingShortName())) {
                    this.buildings.get(j).insertRoom(room);
                }
            }
            //add it to the departments room listing as well
            this.department.addRoom(room);

            //add to university as well
            this.uni.addRoom(room);
        }
    }

    //creates an instructor object and places it in the appropriate arrayList
    private void createInstructor(String departmentName, String firstName, String lastName, String pref) {
        Instructor instructor = new Instructor(departmentName, firstName, lastName, pref);
        this.instructors.add(instructor);
    }

    //creates a course object and places it in the appropriate arrayList
    private void createCourse(String departmentName, String className, String profFirstName, String profLastName) {
        Course course = new Course(departmentName, className, profFirstName, profLastName);
        this.courses.add(course);
    }
    
    //creates a course object and places it in the appropriate arrayList
    private void createCourse(String departmentName, String className, String profFirstName, String profLastName, int expectedEnrollment) {
        Course course = new Course(departmentName, className, profFirstName, profLastName);
        course.setExpectedEnrollment(expectedEnrollment);
        this.courses.add(course);
    }

    //will parse a given line by ", " and place it in the arrayList lineInfo
    private String[] parseLine(String line) {
        String delim = ",";
        String[] stringInfo = line.split(delim);
        return stringInfo;
    }

    //checks if there is a next line in the file
    private boolean hasNextLine() {
        return scanner.hasNextLine();
    }

    //gets the next line in the file
    private String getNextLine() {
        return scanner.nextLine();
    }

    @Override
    public ArrayList<TimePeriod> getTimePeriods() {
        return this.timePeriods;
    }

    @Override
    public int getTimeBetweenPeriods() {
        return this.timeBetweenClasses;
    }

    //returns the department object created from parsing
    @Override
    public Department getDepartment() {
        return this.department;
    }
    
    public static boolean isInteger(String s) {
        try { 
            Integer.parseInt(s); 
        } catch(NumberFormatException e) { 
            return false; 
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    ArrayList<Instructor> instructors = new ArrayList(20);
    ArrayList<Course> courses = new ArrayList(20);
    ArrayList<Building> buildings = new ArrayList(20);
    ArrayList<String> classPreferences = new ArrayList(50);
    ArrayList<String> roomPreferences = new ArrayList(50);
    ArrayList<TimePeriod> timePeriods = new ArrayList(5);
    int timeBetweenClasses = 0;
    Department department;
    University uni;
    FileReaderContext fileReaderContext;
    Scanner scanner;
    File file;
    
}